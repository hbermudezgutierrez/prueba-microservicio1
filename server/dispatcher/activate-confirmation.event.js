const dispatcherEvent = require('../lib/dispatcher');
const eventNameConstants = require('../constants/events-name.constants');

/**
 *  Metodo que hace la publicacion del evento OrderBilled
 * @param {{}} payloadEventOrderBilled 
 */
const publishEventOrderBilled = (payloadEventOrderBilled) => {
    dispatcherEvent.publish(
        eventNameConstants.ORDERBILLED,
        payloadEventOrderBilled.id_process,
        payloadEventOrderBilled
    ).then(() => {
        console.log(`Emite el evento [${eventNameConstants.ORDERBILLED}] para activar el financiamiento del pedido ${payloadEventOrderBilled.numero_pedido} - ${payloadEventOrderBilled.id_process}.`);
    }).catch((err) => {
        console.log(' Se genero un error publicando el evento ', eventNameConstants.ORDERBILLED, '  -  ', err.message);
    });
};

module.exports = {
    publishEventOrderBilled
};