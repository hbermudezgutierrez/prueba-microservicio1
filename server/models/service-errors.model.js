"use strict";

const mongoose = require('mongoose'),
    dbConfig = require("../config/database.config");

const ServiceErrorsSchema = mongoose.Schema({
    success: String,
    origen: String,
    id_process: String,
    message: String,
    documents: Object
}, {
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model('service_errors', ServiceErrorsSchema, dbConfig.service_errors_collection);