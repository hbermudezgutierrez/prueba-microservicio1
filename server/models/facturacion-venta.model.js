"use strict";

const mongoose = require('mongoose'),
    dbConfig = require("../config/database.config");

const facturacionVentaSchema = mongoose.Schema({
    Num_Orden: String,
    Num_pedido_SAP: String,
    Nom_cliente: String,
    Valor: Number,
    Num_factura: String,
    Fecha_emision:String,
    Email: String,
    Tipo_proceso: String,
    Canal: String,
    URL_Factura: String,
    Estado_Proceso: String,
    Fecha_Proceso: String,
    Response_error: String
}, {
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model('coll_facturacion_venta', facturacionVentaSchema, dbConfig.facturacion_venta_collection);