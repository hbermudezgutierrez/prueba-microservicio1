"use strict";

const mongoose = require('mongoose'),
    dbConfig = require("../config/database.config");

    const servicesSchema = mongoose.Schema({
        request:Object,
        response:Object,
        Estado_Proceso: String,
        Fecha_Proceso: String
        
    }, {
        timestamps: true,
        versionKey: false
    });
    
    module.exports = mongoose.model('coll_services_errors', servicesSchema, dbConfig.services_errors_collection);