"use strict";

const mongoose = require('mongoose'),
    dbConfig = require("../config/database.config"),
    file_status = require('../constants/file-sftp-state.constants');

const FileSftpSchema = mongoose.Schema({
    nombre_archivo: String,
    registros_procesados: {
        type: Number,
        default: 0
    },
    estado: {
        type: String,
        default: file_status.ERROR_PROCESANDO
    }
}, {
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model('Files-Sftp', FileSftpSchema, dbConfig.file_sftp_collection);