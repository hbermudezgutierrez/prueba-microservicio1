"use strict";

const mongoose = require('mongoose'),
    dbConfig = require("../config/database.config"),
    order_status = require('../constants/order-billing-state.constants');


const estadosValidos = {
    values: [order_status.CONTABILIZADO, order_status.FACTURADO, order_status.FACTURADO, order_status.NOTIFICADO],
    message: '{VALUE} no es un estado valido'
}

const LogsSchema = mongoose.Schema({
    estado: String,
    createdAt: {
        type: Date,
        default: new Date().toISOString()
    }
}, { _id: false });

const OrdenBillingSchema = mongoose.Schema({
    id_process: String,
    numero_orden: String,
    numero_pedido: String,
    canal: String,
    tipo_venta: String,
    proceso_venta: String,
    tipo_pago: String,
    cliente: {
        nombre: String,
        telefono: String,
        correo: String
    },
    estado: {
        type: String,
        enum: estadosValidos,
        required: [true, 'estado is required'],
        default: order_status.CONTABILIZADO
    },
    factura: {
        numero: String,
        url: Object
    },
    logs_estados: [LogsSchema]
}, {
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model('Orders-Billing', OrdenBillingSchema, dbConfig.order_collection);