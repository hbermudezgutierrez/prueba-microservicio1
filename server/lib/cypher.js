"use strict";

const crypto = require('crypto'),
    gnConfig = require("../config/general.config");
							 
/**
 * función usada para encritar datos usando el alg ASE 128 ECB
 * @param data. Datos a encriptar
 * @returns valor encriptado en formato base 64
 */

const encrypt = (data) => {
    try {
        const encryptionKey = gnConfig.encryption_key;
        const cipher = crypto.createCipheriv('aes-128-ecb', encryptionKey, '');
        cipher.setAutoPadding(true);

        let encrypted = cipher.update(data, 'utf8', 'base64');
        encrypted += cipher.final('base64');			 
        return encrypted;											 
    } catch(e) {
        if (gnConfig.logs) console.error("Error en encrypt >>>:", e);
    }
}

const encryptV2 = (data) => {
    try {
        const encryptionKey = gnConfig.encryption_key_v2;
        const cipher = crypto.createCipheriv('aes-128-ecb', encryptionKey, '');
        cipher.setAutoPadding(true);

        let encrypted = cipher.update(data, 'utf8', 'base64');
        encrypted += cipher.final('base64');			 
        return encrypted;											 
    } catch(e) {
        if (gnConfig.logs) console.error("Error en encrypt >>>:", e);
    }
}

module.exports = {encrypt,encryptV2};
