"use strict";

const EventTrn = require("event-trn"),
  esConfig = require("../config/eventstore.config"),
  uuidv4 = require('uuid').v4;

let eventConfigs = new Array();
let eventDispatcher = null;
  
const listen = () => {
  let eventDispatcher = EventTrn.connect(
      esConfig.nats_cluster_id, 
      esConfig.consumer_group,
      esConfig.nats_servers.split(','), () => {
      
      let eventBuilder = eventDispatcher
                          .handlersBuilder()
                          .group(esConfig.consumer_group);

      eventConfigs.forEach(value => {
        eventBuilder.onEvent(value.eventName, value.fn);
      })
        
      eventBuilder.listen();
  });
}

const registerEvent = (eventName, callback) => {
  eventConfigs.push({ eventName, fn: callback });
}

const publish = (eventId, aggregateId, payload) => {
    const uuid = uuidv4();
    if(eventDispatcher === null){
      eventDispatcher = EventTrn.connect(
          esConfig.nats_cluster_id, 
          esConfig.consumer_group,
          esConfig.nats_servers.split(','), () => {});
    }
    return eventDispatcher.publish(eventId, aggregateId, payload, uuid);
}

module.exports = { publish, listen, registerEvent }
