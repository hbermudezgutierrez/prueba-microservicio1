"use strict";

const axios = require("axios"),
  gnConfig = require("../config/general.config"),
  info = require("../../package.json");

function cacheInicia(data){
  try {
    let resCache = axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: `${gnConfig.ruta_comunes}/comunes/cache`,
      preambleCRLF: true,
      postambleCRLF: true,
      data: data
    })
    .then(r => console.log("r >>>;", r))
    .catch(e => console.log("e >>>;", e))
    return resCache;  
  } catch(e) {
    if (gnConfig.logs) console.error("Error en cacheInicia >>>:", e);
  }
}

function cache(dt){
  try {
    const req = dt.request;
    let body = req.hasOwnProperty(req.method)? req[req.method]: req;

    let obj = {
      state: "EN_PROCESO",
      service: req.service,
      method: req.method,
      url_factura: " ",
      status: req.status || "PENDIENTE",
      retries: req.retries || 0,
      port: req.port || 0,
      serviceid: req.serviceid || info.name,
      task: req.task,
      description: req.description,
      version: req.version || info.version,
      request: body,
      response: dt.response
    }

    console.log("obj en cache:", obj);
    return axios({
      headers: { "content-type": "application/json" },
      method: "put",
      url: `${gnConfig.ruta_comunes}/comunes/cache/${req.id_process}`,
      preambleCRLF: true,
      postambleCRLF: true,
      data: obj
    })
    .then(r => console.log("r cache >>>:", r.data))
    .catch(e => console.log("e cache >>>:", e));
  } catch(e) {
    if (gnConfig.logs) console.error("Error en cache >>>:", e);
  }
}

function audit(data){  
  try {
    let raiz = data.request.raiz;
    let obj_req = { [raiz]: data.request[raiz] };
    let obj = {
      id_process: data.request.id_process,
      service: {
        serviceid: info.name,
        description: info.description,
        version: info.version,
      },
      task: data.request.task,
      status: data.response.success === false? "FINISHED_ERROR" :"FINISHED_OK",
      error: data.response.success === false? data.response.message: "",
      start_date: data.request.startDate,
      end_date: new Date().toISOString(),
      executions : {
        request: obj_req,
        response: data.response
      }
    }
    let resAudit = axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: `${gnConfig.ruta_comunes}/comunes/audit`,
      preambleCRLF: true,
      postambleCRLF: true,
      data: obj
    })
    // .then(r => console.log("r >>>;", r))
    // .catch(e => console.log("e >>>;", e))
    return resAudit;  
  } catch(e) {
    if (gnConfig.logs) console.error("Error en cache >>>:", e);
  }
}

function errors(data){ 
  try {
    let obj = {
      success: data.success,
      origen: data.origen,
      id_process: data.process,
      message: data.message,
      serviceid: info.name,
      documents : data.documents
    }
    let resErrors = axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: `${gnConfig.ruta_comunes}/comunes/errors`,
      preambleCRLF: true,
      postambleCRLF: true,
      data: obj
    });
    return resErrors;  
  } catch(e) {
    if (gnConfig.logs) console.error("Error en cache >>>:", e);
  }
}

function runAll(data){
    if (data.response.success===false) errors(data.response);
    cache(data);
    //audit(data);
}

module.exports = { cacheInicia, cache, audit, errors, runAll }