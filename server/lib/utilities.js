'use strinc';

/**
 * Función para validar si "value" es null
 * @param {*} value 
 * @return true o false
 */
const isNull = function (value) {
    return ((value === null && typeof value === "object") ||
        (value === "null" && typeof value === "string")) ?
        true : false;
}

/**
 * Función para validar si "value" es undefined
 * @param {*} value 
 * @return true o false
 */
const isUndefined = function (value) {
    return ((value === undefined && typeof value === "undefined") ||
        (value === "undefined" && typeof value === "string")) ?
        true : false;
}

/**
 * Función para validar si "value" es empty
 * @param {*} value 
 * @return true o false
 */
const isEmpty = function (value) {
    return (isUndefined(value) ||
        (value === "" && typeof value === "string")) ?
        true : false;
}

/**
 * Función para validar si "value" es null, undefined o vacío
 * La validación toma en cuenta si el null y el undefined 
 * son object o string.
 * @param {*} value valor de entrada
 * @return true o false
 */
const isNullOrEmpty = function (value) {
    return (isNull(value) || isEmpty(value)) ? true : false;
}

/**
 * Función para validar si "value" es null
 * La validación toma en cuenta si el null es object o string.
 * @param {*} value valor de entrada
 * @return cero "0" si es null, sino devuelve value
 */
const ifNullToZero = function (value) {
    return (isNull(value)) ? 0 : value;
}

/**
 * Función para validar si "value" es null
 * La validación toma en cuenta si el null es object o string.
 * @param {*} value valor de entrada
 * @return vacío "" si es null, sino devuelve value
 */
const ifNullToBlank = function (value) {
    return (isNull(value)) ? "" : value;
}

/**
 * Funcion para optener un numero Aleatorio de 10 digitos
 */
const getIDUnique = () => `${Math.floor(Math.random() * 9999999999)}`;

/**
 * Metodo para clonar un objeto que tiene otros objetos como atributos,
 * realiza una copia completa. Evita una copia superficial del objeto. 
 * @param {*} obj objeto que se desea clonar 
 */
const cloneObject = (obj) => JSON.parse(JSON.stringify(obj));

/**
 * Metodo que permite agrupar un arreglo de objetos
 * @param {*} arrayIn arreglo origen para agrupar
 * @param {*} propA propiedad con que se va agrupar
 * @param {*} propB propiedad para mostrar en cada grupo
 */
const groupBy = (arrayIn, propA, propB) => {
    return arrayIn.reduce((groups, item) => {
        (groups[item[propA]] = groups[item[propA]] || [])
            .push({
                [propB]: item[propB]
            });
        return groups;
    }, {});
}

/** 
 * Evalua si el objeto contiene la propiedad dada. Identico a object.hasOwnProperty(key), 
 * pero usa la forma original de la función del propotipo.
 * @param {*} object Objeto objetivo
 * @param {*} key Propiedad a buscar
 */
const has = (object, key) => {
    return object != null && Object.prototype.hasOwnProperty.call(object, key);
}

/**
 * Función que permite agregar una nueva propiedad a un objeto
 * Solo si la variable existe y trae algun valor
 * @param {*} object - objeto origen
 * @param {*} property - clave o nombre de la propiedad en el objeto
 * @param {*} value - valor que se debe asignar a la propiedad
 */
const setIfNotEmpty = function (object, property, value) {
    if (value && !isNullOrEmpty(value)) object[property] = value;
    return object;
};

/**
 * 
 * Obtiene el objeto solicitado que se encuentra dentro de otro objeto
 * si existe la estructura, sino deveulve el original
 * @param {*} src objeto origen con otros objetos internos
 * @param {*} name nombre del objeto que se quiere extraer del origen
 */
const extractObjectFrom = (src, name) => (src && has(src, name)) ? src[name] : src;

/**
 * Funcion para validar si un campo viene vacío o null si lo es,
 * retorna la salida "o", si no, devuelve el mismo valor "i".
 * @param {*} i valor de entrada 
 * @param {*} o valor de salida
 */
const ifEmpty = (i, o) => (i && isNullOrEmpty(i)) ? o : i;

/**
 * Retorna el objecto interno si existe.
 * Con ECMAScript 2020 se puede reemplazar con "optional chaining".
 * @param {*} obj 
 * @param  {...any} args 
 */
const getNested = (obj, ...args) => {
    return args.reduce((obj, level) => obj && obj[level], obj);
}

/**
 * Metodo para validar si un "value" es un array
 * @param {*} value argumento a evaluar
 * @return true o false
 */
const isArray = (value) => value instanceof Array

/**
 * Metodo para validar si un "value" es un objeto
 * @param {*} value argumento a evaluar
 * @return true o false
 */
const isObject = (value) => value instanceof Object


/**
 * Metodo para validar si un "array" contiene elementos, si no esta vacio
 * @param {*} array argumento a evaluar
 * @return true o false
 */
const containsElements = (array) => Array.isArray(array) && array.length > 0;


/**
 * Busca una propiedad de forma recursiva en un objeto y devuelve su
 * valor, undefined si la propiedad no existe. Soporta propiedades anidadas en otros objetos.
 * @param {*} obj objeto.
 * @param {*} prop propiedad.
 */
const findPropertyValue = (obj, prop) => {
    return Object.keys(obj).reduce((value, key) => {
        if (value)
            return value;
        if (prop === key)
            return obj[key];
        else if (isObject(obj[key]))
            return findPropertyValue(obj[key], prop, value);
    }, undefined);
};


const isEmptyElement = (object) => {
    return isNullOrEmpty(object) || Object.entries(object) == 0;
}

module.exports = {
    getIDUnique,
    cloneObject,
    groupBy,
    has,
    setIfNotEmpty,
    extractObjectFrom,
    isNull,
    isUndefined,
    isEmpty,
    isNullOrEmpty,
    ifNullToZero,
    ifNullToBlank,
    ifEmpty,
    getNested,
    isArray,
    isObject,
    containsElements,
    findPropertyValue,
    isEmptyElement,
};