'use strict';

const axios = require("axios"),
  venta = require("../services/facturacion-venta.service"),
  info = require("../../package.json"),
  gnConfig = require("../config/general.config"),
  { numeroFacturaSapBAPI, billingdocGetDetail } = require('../routes/rfc-sap');


async function getCacheDB(params) {
  try {
    const { data } = await axios({
      headers: { "content-type": "application/json" },
      method: "get",
      url: `${gnConfig.ruta_comunes}/comunes/cache${params}`,
      preambleCRLF: true,
      postambleCRLF: true
    });
    return data;
  } catch (e) {
    if (gnConfig.logs) console.log("Error en getCacheDB:", e);
  }
}

function getReqGenerales(dt) {
  dt.serviceid = info.name;
  dt.version = info.version;
  dt.description_ms = info.description;
  dt.startDate = new Date().toISOString();
  return dt;
}

async function getReqPDF(dt) {
  try {
    let obj = dt;
    let js = {};
    const data = await getCacheDB(`?id_process=${dt.id_process}`);

    if (data !== undefined && data.found !== 0) {
      let cachedb = data.documents[0].cache;
      js.numero_factura = cachedb.pedidos[0].numero_factura;
      js.tipo_cliente = cachedb.pedidos[0].tipo_cliente;
    }
    obj[obj.method] = js;

    console.log("obj en getReqPDF", obj);
    return obj;
  } catch (e) {
    if (gnConfig.logs) console.error("Error en getReqPDF:", e);
  }
}

async function getURLPDFDocumentByBill(req) {
  let response = "";
  try {
    console.log("Request inicial para getURLPDFDocumentByBill >>>", req)
    if (req.info_factura.Canal == gnConfig.canal_factura_venta) {
      let responseRFC = await venta.requestSAPSendByBill(req);
      console.log("Request inicial RFC SAP >>> ", JSON.stringify(responseRFC))
      let resSAP = await numeroFacturaSapBAPI(responseRFC);
      if (resSAP.success == true) {
        let numero_factura = resSAP.documents[0].NUMERO_FACTURA;
        console.log("Numero de factura obtenido en SAP >>> ", numero_factura)
        let resGetURLV2 = await venta.getURLV2(numero_factura)
        //  let resGetURLV2= await venta.getURLV2("7910001308")
        let URL = resGetURLV2.url
        let ERROR = resGetURLV2.error.isError
        if (ERROR == false && URL !== "") {
          console.log("URL obtenida >>> ", URL)
          response = {
            success: true,
            message: "Execution successfully",
            origen: "/ms-billing/getURLPDFDocumentByBill",
            status: 200,
            documents: {
              URL: URL
            }
          }
        } else {
          console.log("Ocurrio un error al solicitar la URL >>>", resGetURLV2)
          response = {
            success: false,
            origen: "/ms-billing/getURLPDFDocumentByBill",
            status: 500,
            message: "Ocurrió un error del lado del servidor"
          }
        }
      } else {
        console.log("Ocurrio un error al solicitar la RFC de SAP >>>", resSAP)
        response = {
          success: false,
          origen: "/ms-billing/getURLPDFDocumentByBill",
          status: 500,
          message: "Ocurrió un error del lado del servidor"
        }
      }
    } else {
      console.log("El canal ingresado no es valido")
      response = {
        success: false,
        origen: "/ms-billing/getURLPDFDocumentByBill",
        status: 500,
        message: "Ocurrió un error del lado del servidor"
      }
    }
  } catch (e) {
    console.log("Error general >>>", e)
    response = {
      success: false,
      origen: "/ms-billing/getURLPDFDocumentByBill",
      status: 500,
      message: "Ocurrió un error del lado del servidor"

    }

  } finally {
    return response
  }
}

async function sendByBill(req) {
  try {
    if (req.info_factura.Canal == gnConfig.canal_factura_venta) {
      console.log("Ingreso a sendByBill >>>>>>>>>")
      let requestSendByBill = await venta.requestSendByBill(req);
      let resFind = await venta.findSendByBill(requestSendByBill);
      console.log("Respuesta busqueda en base de datos", resFind.documents.length)
      // PruebaSend(req)
      if (resFind.success == true && resFind.documents.length) {
        sendBill(req)
        let idProcess = resFind.documents[0]._id
        console.log("Se encontro un registro en la base de datos")
        let response = {
          success: true,
          origen: "/ms-billing/SendByBill",
          message: "Execution successfully",
          documents: {
            transactionId: idProcess
          }
        }
        return response
      } else {
        let resFind = await venta.createSendByBill(requestSendByBill)
        console.log("resFind", resFind)
        sendBill(req)
        let idProcess = resFind.documents[0]._id
        console.log("No se encontro un registro en la base de datos")
        let response = {
          success: true,
          origen: "/ms-billing/SendByBill",
          message: "Execution successfully",
          documents: {
            transactionId: idProcess
          }
        }
        return response
      }
    } else {
      console.log("El canal ingresado no es valido")
      let response = {
        success: false,
        origen: "/ms-billing/SendByBill",
        message: 'El canal ingresado no es valido debe ser ‘ECOM’'
      }
      await venta.createServiceErros(req, response, gnConfig.estado_error)
      return response
    }


  } catch (e) {
    console.log("Error general en la aplicacion >>>", e)
    let response = {
      success: false,
      origen: "/ms-billing/SendByBill",
      message: "Error General"
    }
    return response
  }
}

async function sendBill(req) {
  try {
    console.log("Request para SAP >>>", req)
    let requestSendByBill = await venta.requestSendByBill(req);
    let responseRFC = await venta.requestSAPSendByBill(req);
    let resSAP = await numeroFacturaSapBAPI(responseRFC);
    if (resSAP.success == true) {
      console.log("Actualiza el estado en la coleccion a >>> ", gnConfig.estado_sap)
      console.log("Numero de factura obtenido en SAP >>>", resSAP.documents[0].NUMERO_FACTURA)
      requestSendByBill.Num_factura = resSAP.documents[0].NUMERO_FACTURA;
      requestSendByBill.Fecha_emision = resSAP.documents[0].FECHA_FACTURA;
      let resRFC_BillingdocGetdetail = await venta.requestBillingdocGetdetail(resSAP.documents[0].NUMERO_FACTURA),
        res_BillingdocGetdetail = await billingdocGetDetail(resRFC_BillingdocGetdetail);
      if (res_BillingdocGetdetail.success == true) {
        requestSendByBill.Valor = res_BillingdocGetdetail.documents[0].NET_VALUE
          + res_BillingdocGetdetail.documents[0].TAX_VALUE;
        console.log("RequestSendByBill para almacenar en la base de datos >>>", requestSendByBill)
        console.log("Actualiza el estado en la coleccion a >>> ", gnConfig.estado_sap)
        await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_sap)
        let resGetURLV2 = await venta.getURLV2(requestSendByBill.Num_factura)
        let URL = resGetURLV2.url
        let ERROR = resGetURLV2.error.isError
        if (ERROR == false && URL !== "") {
          console.log("Url obtenida >>>", URL)
          requestSendByBill.URL_Factura = URL
          console.log("RequestSendByBill para almacenar en la base de datos >>>", requestSendByBill)
          console.log("Actualiza el estado en la coleccion a >>> ", gnConfig.estado_generarURL)
          await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_generarURL)
          let reqCorreo = await venta.requestEnvioCorreo(requestSendByBill);
          console.log("REQUEST CORREO", reqCorreo);
          let resSendEmail = await venta.sendEmailSendByBill(reqCorreo);
          if (resSendEmail.status == 200 || (resSendEmail.documents.isValid == 'true' && resSendEmail.documents.message == 'Is Valid: ')) {
            console.log("Actualiza el estado en la coleccion a >>> ", gnConfig.estado_notificado)
            await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_notificado)
          } else {
            console.log("Respuesta Fallida sendEmailSendByBill >>>", resSendEmail)
            requestSendByBill.Response_error = `sendEmail: ${resSendEmail.message}`
            await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_error)
            await venta.createServiceErros(reqCorreo,
              { origen: 'sendEmail', message: 'Servidor de correo no responde (sendEmailSendByBill)'},
              gnConfig.estado_error);
          }
          console.log("Termina proceso >>>")

        } else {
          console.log("Respuesta Fallida URL >>>", resGetURLV2)
          requestSendByBill.Response_error = `getUrlBill: ${resGetURLV2.error.msg}`
          await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_error)
          let info_factura = req.info_factura;
          let numFactura = requestSendByBill.Num_factura;
          await venta.createServiceErros({info_factura, numFactura},
            { origen: 'getUrlBill', message: 'No se encontró factura en paradigma (url_paradigma2)'},
            gnConfig.estado_error);
        }
      } else {
        console.log("Respuesta Fallida billingdocGetDetail >>>", res_BillingdocGetdetail);
        requestSendByBill.Response_error = `billingdocGetDetail: ${res_BillingdocGetdetail.message}`
        await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_error)
        resRFC_BillingdocGetdetail['info_factura'] = req.info_factura;
        await venta.createServiceErros(resRFC_BillingdocGetdetail,
          { origen: 'billingdocGetDetail', message: 'No se encontró detalle de la factura en sap (billingdocGetDetail - /sap/invoke)'},
          gnConfig.estado_error);
      }
    } else {
      console.log("Respuesta no esperada en SAP >>>", resSAP)
      requestSendByBill.Response_error = `sapBAPI: ${resSAP.message}`
      await venta.UpdateSendByBill(requestSendByBill, gnConfig.estado_error)
      await venta.createServiceErros(responseRFC,
        { origen: 'sapBAPI', message: 'No se ha generado factura SAP (numeroFacturaSapBAPI - /sap/invoke)'},
        gnConfig.estado_error);
    }

  } catch (e) {
    console.log("Error general en la aplicacion >>>", e)
  }
}




module.exports = {
  getReqGenerales,
  getReqPDF,
  getURLPDFDocumentByBill,
  sendByBill,
}