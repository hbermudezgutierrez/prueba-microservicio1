"use strict";

const validate = require("../lib/validate"),
    gnConfig = require("../config/general.config");
    
function errorHandler(e, r) {
    if (gnConfig.logs) console.log("Error errorHandler > ", e);
    
    if(e.name === 'ValidationError')
      r.status = 400;
    else if (e.status)
      r.status = e.status;
    else
      r.status = 500;
  
    if (!validate.isNullOrEmpty(e.response) && !validate.isNullOrEmpty(e.response.data)) {
      r.status = e.response.status;
      r.message = e.response.data.title;
      r.documents = {
        errors: e.response.data.errors
      }
    } else {
      r.message = e.message || e.stack;
    }
    r.success = false;
    return r;
  }
  
  module.exports = { errorHandler };