"use strict";

const yup = require("yup");

const RegExp = {
  hexadecimal: /^[a-fA-F\d\-]+$/,
  string_number: /^[a-zA-Z0-9]+$/,
  tipo_cliente: /^(R|C|P|F|S)$/,
  string_special_1: /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\-\.\s]+$/,
  string_special_2: /^[a-zA-ZáéíóúÁÉÍÓÚñÑ\-\.\,\_\s]+$/,
  string_special_3: /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\-\.\,\_\:\;\#\s]+$/,
  string_special_4: /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ\-\.\s]+$/,
  date_format_iso: /^((\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3})Z)*$/,
  string_under: /^[a-zA-Z0-9\_\s]+$/,
  estado_entrega: /^(08_ENTREGADO_CLIENTE)$/,
  mail: /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/,
  tipo_pago: /^(CONTADO|FINANCIADO)$/,
  email: /(^$|^.*@.*\..*$)/,
}

function postParadigma(data) {
  const schema = yup.object().shape({
    id_process: yup
      .string()
      .required()
      .min(5)
      .max(24)
      .matches(RegExp.hexadecimal, {
        message: `id_process [${data.id_process
          }] does not contain the valid format`
      }),
    numero_factura: yup
      .string()
      .required()
      .min(1)
      .max(15)
      .matches(RegExp.string_number, {
        message: `numero_factura [${data.numero_factura}] does not contain the valid format`
      }),
    tipo_cliente: yup
      .string()
      .required()
      .length(1)
      .matches(RegExp.tipo_cliente, {
        message: `tipo_cliente [${data.tipo_cliente}] is not a valid type`
      })
  });

  schema.validateSync(data);
}

function isNullOrEmpty(v) {
  let r = false;
  if (v === null && typeof v === "object") {
    r = true;
  } else if (v === "null" && typeof v === "string") {
    r = true;
  } else if (v === undefined && typeof v === "undefined") {
    r = true;
  } else if (v === "undefined" && typeof v === "string") {
    r = true;
  } else if (v === "" && typeof v === "string") {
    r = true;
  }
  return r;
}

function validateSAPNumeroFactura(data) {

  const schema = yup.object().shape({
    functionName: yup
      .string()
      .required()
      .min(1)
      .max(25)
      .matches(RegExp.string_special_3, {
        message: `functionName does not contain the valid format`
      }),
    importParameterList: yup.object().shape({
      ET_PEDIDOS: yup.array().of(yup.object().shape({
        VBELN: yup
          .string()
          .required()
          .min(1)
          .max(20)
          .matches(RegExp.string_number, {
            message: `importParameterList.VBELN does not contain the valid format`
          }),
        POSEX: yup
          .string()
          .required()
          .min(1)
          .max(10)
          .matches(RegExp.string_number, {
            message: `importParameterList.POSEX does not contain the valid format`
          })
      }))
    })
  });

  schema.validateSync(data);

}

function validateEventDeliveryEntrega(data) {
  const schema = yup.object().shape({
    aggregateId: yup
      .string()
      .required()
      .min(1)
      .max(30)
      .matches(RegExp.hexadecimal, { message: `aggregateId does not contain the valid format` }),
    orderStatus: yup.object().required().shape({
      estado: yup
        .string()
        .required()
        .max(20)
        .matches(RegExp.estado_entrega, { message: `estado does not contain the valid format` }),
      subEstado: yup
        .string()
        .max(25)
        .matches(RegExp.string_under, { message: `subEstado does not contain the valid format` }),
      fechaActualizacion: yup
        .string()
        .required()
        .min(1)
        .max(25)
        .matches(RegExp.date_format_iso, { message: `fechaActualizacion does not contain the valid format` })
    }),
  });

  schema.validateSync(data);
}

function validateFacturaSapBAPI(data) {

  const schema = yup.object().shape({
    functionName: yup
      .string()
      .required()
      .min(1)
      .max(30)
      .matches(RegExp.string_special_3, {
        message: `functionName does not contain the valid format`
      }),
    tables: yup.object().shape({
      SALES_DOCUMENTS: yup.array().min(1).of(yup.object().shape({
        VBELN: yup
          .string()
          .required()
          .min(1)
          .max(20)
          .matches(RegExp.string_number, {
            message: `SALES_DOCUMENTS.VBELN does not contain the valid format`
          })
      }))
    }),
    structures: yup.object().shape({
      I_BAPI_VIEW: yup.object().shape({
        paramList: yup.object().shape({
          FLOW: yup.string()
            .max(1)
            .required()
            .matches(RegExp.string_special_3, {
              message: `FLOW does not contain the valid format`
            })
        })
      })
    })
  });

  schema.validateSync(data);

}

function validateSendByBilla(data) {
  const schema = yup.object().shape({
    Num_Orden: yup
      .string()
      .required()
      .min(5)
      .max(24)
      .matches(RegExp.string_number, {
        message: `Num_Orden [${data.Num_Orden
          }] does not contain the valid format`
      }),
    Num_pedido_SAP: yup
      .string()
      .required()
      .matches(RegExp.string_number, {
        message: `Num_pedido_SAP [${data.Num_pedido_SAP}] does not contain the valid format`
      }),
    Nom_cliente: yup
      .string()
      .required()
      .matches(RegExp.string_special_4, {
        message: `Nom_cliente [${data.Nom_cliente}] does not contain the valid format`
      }),
    Email: yup
      .string()
      .required()
      .matches(RegExp.mail, {
        message: `Email [${data.Email}] does not contain the valid format`
      }),
    Tipo_proceso: yup
      .string()
      .required()
      .matches(RegExp.string_special_4, {
        message: `Tipo_proceso [${data.Tipo_proceso}] does not contain the valid format`
      }),
    Canal: yup
      .string()
      .required()
      .matches(RegExp.string_special_4, {
        message: `Canal [${data.Canal}] does not contain the valid format`
      })
  });

  schema.validateSync(data);
}

function validateSendByBill(data) {
  const schema = yup.object().shape({
    info_factura: yup.object().shape({
      Num_Orden: yup
        .string()
        .required()
        .matches(RegExp.string_number, {
          message: `Num_Orden [${data.Num_Orden
            }] does not contain the valid format`
        }),
      Num_pedido_SAP: yup
        .string()
        .required()
        .matches(RegExp.string_number, {
          message: `Num_pedido_SAP [${data.Num_pedido_SAP}] does not contain the valid format`
        }),
      Nom_cliente: yup
        .string()
        .required()
        .matches(RegExp.string_special_4, {
          message: `Nom_cliente [${data.Nom_cliente}] does not contain the valid format`
        }),
      Email: yup
        .string()
        .required()
        .matches(RegExp.mail, {
          message: `Email [${data.Email}] does not contain the valid format`
        }),
      Tipo_proceso: yup
        .string()
        .required()
        .matches(RegExp.string_special_4, {
          message: `Tipo_proceso [${data.Tipo_proceso}] does not contain the valid format`
        }),
      Canal: yup
        .string()
        .required()
        .matches(RegExp.string_special_4, {
          message: `Canal [${data.Canal}] does not contain the valid format`
        })
    }),
  });
  schema.validateSync(data)
}

function validateGetURLPDFDocByBill(data) {
  const schema = yup.object().shape({
    info_factura: yup.object().shape({
      Num_Orden: yup
        .string()
        .required()
        .matches(RegExp.string_number, {
          message: `Num_Orden [${data.Num_Orden
            }] does not contain the valid format`
        }),
      Num_pedido_SAP: yup
        .string()
        .required()
        .matches(RegExp.string_number, {
          message: `Num_pedido_SAP [${data.Num_pedido_SAP}] does not contain the valid format`
        }),
      Tipo_proceso: yup
        .string()
        .required()
        .matches(RegExp.string_special_4, {
          message: `Tipo_proceso [${data.Tipo_proceso}] does not contain the valid format`
        }),
      Canal: yup
        .string()
        .required()
        .matches(RegExp.string_special_4, {
          message: `Canal [${data.Canal}] does not contain the valid format`
        })
    }),
  });
  schema.validateSync(data)
}


function validateReqOrderBilling(data) {
  const schema = yup.object().shape({
    id_process: yup
      .string()
      .required()
      .matches(RegExp.hexadecimal, {
        message: `id_process [${data.id_process}] does not contain the valid format`
      }),
    numero_orden: yup
      .string()
      .required()
      .matches(RegExp.string_number, {
        message: `numero_orden [${data.numero_pedido}] does not contain the valid format`
      }),
    numero_pedido: yup
      .string()
      .required()
      .matches(RegExp.string_number, {
        message: `numero_pedido [${data.numero_pedido}] does not contain the valid format`
      }),
    canal: yup
      .string()
      .required()
      .matches(RegExp.string_under, {
        message: `canal [${data.canal}] does not contain the valid format`
      }),
    tipo_venta: yup
      .string()
      .required()
      .matches(RegExp.string_special_1, {
        message: `tipo_venta [${data.tipo_venta}] does not contain the valid format`
      }),
    proceso_venta: yup
      .string()
      .required()
      .matches(RegExp.string_special_1, {
        message: `proceso_venta [${data.proceso_venta}] does not contain the valid format`
      }),
    tipo_pago: yup
      .string()
      .matches(RegExp.tipo_pago, {
        message: `tipo_pago [${data.tipo_pago}] does not contain the valid format`
      }),
    cliente: yup.object().shape({
      nombre: yup
        .string()
        .required()
        .matches(RegExp.string_special_1, {
          message: `nombre [${data.nombre}] does not contain the valid format`
        }),
      telefono: yup
        .string()
        .required()
        .matches(RegExp.string_number, {
          message: `telefono [${data.telefono}] does not contain the valid format`
        }),
      correo: yup
        .string()
        .matches(RegExp.email, {
          message: `correo [${data.correo}] does not contain the valid format`
        })
    })
  });

  schema.validateSync(data);
}


function eventOrderDeliveredCustomer(data) {
  const schema = yup.object().shape({
    channelOrderNbr: yup
      .string()
      .required()
      .min(3)
      .max(50)
      .matches(RegExp.string_number, { message: `channelOrderNbr does not contain the valid format` }),
    products: yup.array().of(yup.object().shape({
      quantity: yup
        .number()
        .required()
        .min(1)
        .max(3),
      position: yup
        .string()
        .required()
        .min(1)
        .max(6)
        .matches(RegExp.string_number, { message: `position does not contain the valid format` }),
      productInfo: yup.object().shape({
        channelProductCode: yup
          .string()
          .required()
          .min(0)
          .max(50)
          .matches(RegExp.string_number, {
            message: `channelProductCode does not contain the valid format`
          }),
        productType: yup
          .string()
          .required()
          .min(0)
          .max(50)
          .matches(RegExp.string_under, {
            message: `productType does not contain the valid format`
          })
      }),
      sapInfo: yup.object().shape({
        sapOrderId: yup
          .string()
          .required()
          .min(0)
          .max(50)
          .matches(RegExp.string_number, { message: `sapOrderId does not contain the valid format` }),
        sapDeliveryId: yup
          .string()
          .required()
          .min(0)
          .max(50)
          .matches(RegExp.string_number, { message: `sapDeliveryId does not contain the valid format` })
      }),
      serials: yup.array().of(yup.string()).required().min(1)
    })).required().min(2)
  });

  schema.validateSync(data);
}


module.exports = {
  postParadigma,
  isNullOrEmpty,
  validateSAPNumeroFactura,
  validateEventDeliveryEntrega,
  validateFacturaSapBAPI,
  validateSendByBill,
  validateGetURLPDFDocByBill,
  validateReqOrderBilling,
  eventOrderDeliveredCustomer,
};
