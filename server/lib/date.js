"use strict";

const pad = (n) => { return (n < 10 ? '0' : '') + n }

/**
 * parametro fstr: %Y - anno , %m - mes, %d - dia, %H - hora, %M - minuto, %S - segundo
 * parametro utc: true, calcula la fecha y hora local 
 */
Date.prototype.format = function (fstr, utc) {
    var that = this;
    utc = utc ? 'getUTC' : 'get';
    return fstr.replace(/%[YmdHMS]/g, function (m) {
        switch (m) {
            case '%Y': return that[utc + 'FullYear']();
            case '%m': m = 1 + that[utc + 'Month'](); break;
            case '%d': m = that[utc + 'Date'](); break;
            case '%H': m = that[utc + 'Hours'](); break;
            case '%M': m = that[utc + 'Minutes'](); break;
            case '%S': m = that[utc + 'Seconds'](); break;
            default: return m.slice(1);
        }
        return ('0' + m).slice(-2);
    });
}

Object.defineProperty(Date.prototype, 'YYYYMMDDHHMMSS', {
    value: function () {
        return this.getFullYear() +
            pad(this.getMonth() + 1) +
            pad(this.getDate()) +
            pad(this.getHours()) +
            pad(this.getMinutes()) +
            pad(this.getSeconds());
    }
})

Object.defineProperty(Date.prototype, 'YYYYMMDD', {
    value: function () {
        return this.getFullYear() +
            pad(this.getMonth() + 1) +
            pad(this.getDate());
    }
})

/**
 * Toma una fecha y devuelve un string que representa la fecha recibida.
 * La información devuelta estará en formato YYYY-MM-DD
 * @param {*} date fecha que recibe para formatear 
 */
const formatToYYYYMMDD = (date) => {
    return `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())}`;
}

/**
 * Toma una fecha y devuelve un string que representa la fecha recibida.
 * La información devuelta estará en formato YYYY-MM-DD
 * @param {*} date fecha que recibe para formatear 
 */
const formatToYYYYMMDD2 = (date) => {
    return `${date.getFullYear()}${pad(date.getMonth() + 1)}${pad(date.getDate())}`;
}

/**
 * Toma una fecha y devuelve un string que representa la fecha recibida.
 * La información devuelta estará en formato YYYY/MM/DD
 * @param {*} date fecha que recibe para formatear 
 */
const formatToYYYYMMDD3 = (date) => {
    return `${date.getFullYear()}/${pad(date.getMonth() + 1)}/${pad(date.getDate())}`;
}

/**
 * Devuelve la fecha actual en formato YYYYMMDD sin separaciones
 */
function getDateYYYYMMDD() {
    return new Date().YYYYMMDD();
}

/**
 * Devuelve la fecha actual en formato YYYYMMDDHHMMSS sin separaciones
 */
function getDateYYYYMMDDHHMMSS() {
    return new Date().YYYYMMDDHHMMSS();
}

/**
 * Devuelve la fecha actual con formato y separaciones
 * @param {*} fstr: formato de la fecha
 * @param {*} utc: true o false, calcula la fecha y hora local 
 */
function getDateFormat(fstr, utc) {
    return new Date().format(fstr, utc);
}

/**
 * Devuelve una fecha a la que se ha agregado un intervalo de tiempo especificado.
 * @sintaxis  DateAdd ( interval, number, date )
 * @param {*} interval String. Intervalo de tiempo que desea agregar.
 * @param {*} number Integer. Número de intervalos que desea agregar. Positivo para agegar o negativo restar.
 * @param {*} date Date o String. Rpresenta la fecha a la que se agrega el intervalo.
 * @return  Puede devolver fechas en el futuro o fechas en el pasado.
 */
const DateAdd = (interval = 's', number = 0, date = Date.now()) => {
    if (interval && number && date) {
        let fecha = new Date(date);
        switch (interval) {
            case 'y': fecha.setFullYear(fecha.getFullYear() + number); break;
            case 'm': fecha.setMonth(fecha.getMonth() + number); break;
            case 'd': fecha.setDate(fecha.getDate() + number); break;
            case 'H': fecha.setHours(fecha.getHours() + number); break;
            case 'M': fecha.setMinutes(fecha.getMinutes() + number); break;
            case 'S': fecha.setSeconds(fecha.getSeconds() + number); break;
            default: return fecha;
        }
        return fecha;
    }
}

/**
 * Toma una fecha y devuelve un string que representa la fecha recibida.
 * La información devuelta estará en formato YYYY-MM-DD HH:MM:SS
 * @param {*} date fecha que recibe para formatear 
 */
const formatToDDMMYYYYHHMMSS = (date) => {
    return `${pad(date.getDate())}-${pad(date.getMonth() + 1)}-${date.getFullYear()} ${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
};


const formatToYYYYMMDDHHMMSS = (date) => {
    return `${date.getFullYear()}-${pad(date.getMonth() + 1)}-${pad(date.getDate())} ${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
};


const millisToMinutesAndSeconds = (millis) => {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return `${minutes}:${(seconds < 10 ? "0" : "")}${seconds}`;
}

const minutesTomillis = (minutes) => {
    return Math.floor(Number(minutes) * 60000);
}

const formatHHMMSS = (date) => {
    return `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
};

module.exports = {
    getDateYYYYMMDD,
    getDateYYYYMMDDHHMMSS,
    getDateFormat,
    formatToYYYYMMDD,
    DateAdd,
    formatToYYYYMMDD2,
    formatToYYYYMMDD3,
    formatToDDMMYYYYHHMMSS,
    millisToMinutesAndSeconds,
    minutesTomillis,
    formatHHMMSS,
    formatToYYYYMMDDHHMMSS,
}