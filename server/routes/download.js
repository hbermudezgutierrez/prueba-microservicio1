"use strict";

const axios = require("axios"),
  gnConfig = require("../config/general.config"),
  msg = require("../class/message"),
  trace = require("../lib/trace"),
  cypher = require("../lib/cypher"),
  error = require("../lib/error");

const orchParadigma = async req => {
  const result = new msg.MessageBuilder()
    .setOrigen(`${req.service}/${req.method}`)
    .setProcess(req.id_process)
    .build();
    let data = {"request": req};
  try {    
    console.log("dta orchParadigma:", data); 
    const obj = req.hasOwnProperty("pdf")? req.pdf: req;
    result.process = obj.id_process;
    result.status = 400;
    data.request.status = "FALLIDO";

    obj.token = cypher.encrypt(`${obj.numero_factura}|${obj.tipo_cliente}`);

    let re = await axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: gnConfig.url_paradigma,
      preambleCRLF: true,
      postambleCRLF: true,
      data: JSON.stringify({ data: obj.token })
    });

    if (JSON.parse(re.data.d).error.isError === true) {
      result.status = 404;
      throw new Error(JSON.parse(re.data.d).error.msg);
    }
    
    if (JSON.parse(re.data.d).url !== ""){
      result.status = 200;
      result.success = true;
      result.message = "Execution successful";
      result.documents = {
        url: JSON.parse(re.data.d).url || "NOT FOUND"
      };
      data.request.status = "COMPLETO";
    }else{
      result.success = false;
      result.documents = {
        url: "pdf invoice not found"
      };
    }
  } catch (e) {
    result = error.errorHandler(e, result);
    data.request.status = "FALLIDO";
  } finally{
    data.response = result;
    trace.runAll(data);
    return result;
  }
};

module.exports = { orchParadigma };
