"use strict";

const gnConfig = require("../config/general.config"),
    axios = require("axios"),
    msg = require("../class/message"),
    trace = require("../lib/trace"),
    error = require("../lib/error"),
    { cloneObject, has, isNullOrEmpty, setIfNotEmpty, isArray } = require("../lib/utilities"),
    { validateSAPNumeroFactura, validateFacturaSapBAPI } = require('../lib/validate'),
    SapInspira = require('../job/models/sap-inspira.model'),
    ValuesVenta = require('../constants/order-tipo-proceso-venta.constants'),
    { updateStatusPresale } = require('../services/index');

/**
 * 
 * @param {*} dt 
 */
async function getRequestNumFactura(dt) {
    try {
        let obj = cloneObject(dt);
        let js = require('../json/factura.json');

        js.credentials.user = gnConfig.user_sap_inspira;
        js.credentials.password = gnConfig.pass_sap_inspira;

        let paramsOrden = {};
        setIfNotEmpty(paramsOrden, 'id_process', dt.id_process);

        let orderDB = await getOrder(paramsOrden);

        if (has(orderDB, 'documents') && !isNullOrEmpty(orderDB.documents[0])) {
            let orden = orderDB.documents[0];
            obj.numero_orden = orden.num_orden;
            js.importParameterList.ET_PEDIDOS = [];

            // mapeo de los pedidos de la orden
            for (const pedido of orden.pedidos) {
                let pedidoFacturar;
                if (has(pedido, 'numero_pedido') && !isNullOrEmpty(pedido.numero_pedido)) {
                    if (!isNullOrEmpty(obj.request.productos)) {
                        let cont = 0;
                        for (let objectResult of obj.request.productos) {
                            if (has(objectResult, 'sim')) {
                                pedidoFacturar = {
                                    VBELN: pedido.numero_pedido,
                                    POSEX: obj.request.productos[cont].sim.posicion
                                };
                                js.importParameterList.ET_PEDIDOS.push(pedidoFacturar);
                            };
                            if (has(objectResult, 'equipo')) {
                                pedidoFacturar = {
                                    VBELN: pedido.numero_pedido,
                                    POSEX: obj.request.productos[cont].equipo.posicion
                                };
                                js.importParameterList.ET_PEDIDOS.push(pedidoFacturar);
                            };
                            cont++;
                        }
                    } else {
                        let cont = 0;
                        for (const producto of pedido.productos) {
                            cont++;
                            pedidoFacturar = {
                                VBELN: pedido.numero_pedido,
                                POSEX: '00000' + cont //'000001''
                            };
                            js.importParameterList.ET_PEDIDOS.push(pedidoFacturar);
                        };
                    };
                };
                // js.importParameterList.ET_PEDIDOS.push(pedidoFacturar);
            };

            obj.factura = js;
            return obj;

        } else {
            //lanzar error
            throw new Error(`No se encontro orden para el id_process - ${dt.id_process}`);
        }
    } catch (e) {
        if (gnConfig.logs) console.error("Error en getRequestNumFactura:", e);
        trace.errors({
            success: false,
            origen: "rfc-sap/getRequestNumFactura",
            id_process: dt.id_process,
            message: "Error al armar el request para generar factura en SAP",
            documents: { error: e, request: dt }
        });
    }
}

/**
 * 
 * @param {*} dta 
 */
async function getRequestContabilizacion(dta) {

    let pl = require('../json/contabilizacion.json');
    pl.credentials.user = gnConfig.user_sap_inspira;
    pl.credentials.password = gnConfig.pass_sap_inspira;
    pl.importParameterList.I_PEDIDO_SERIAL = [];

    if (isArray(dta.request.productos) && !isNullOrEmpty(dta.request.productos)) {
        for (let objResult of dta.request.productos) {
            let pedido;
            if (has(objResult, "equipo")) {
                pedido = {
                    VBELN: dta.request.num_pedido,
                    POSEX: objResult.equipo.posicion,
                    SERNR: objResult.equipo.imei
                };
                pl.importParameterList.I_PEDIDO_SERIAL.push(pedido);
            }
            if (has(objResult, "sim")) {
                pedido = {
                    VBELN: dta.request.num_pedido,
                    POSEX: objResult.sim.posicion,
                    SERNR: objResult.sim.serial
                };
                pl.importParameterList.I_PEDIDO_SERIAL.push(pedido);
            }
        }
    } else {
        throw new Error(`No se encuentra informacion de productos`);
    }

    return pl;
}

/**
 * Funcion que crea el request para consumo de RFC BAPI
 * @param {*} dt id_process
 * @returns Request consumo RFC
 */
async function getReqFacturaBAPI(dt) {

    let obj = cloneObject(dt);
    let js = require('../json/factura_bapi.json');

    js.credentials.user = gnConfig.user_sap_inspira;
    js.credentials.password = gnConfig.pass_sap_inspira;

    let paramsOrden = { id_process: dt.id_process };

    let orderDB = await getOrder(paramsOrden);

    if (!isNullOrEmpty(orderDB) && orderDB.success && has(orderDB, 'documents')
        && !isNullOrEmpty(orderDB.documents[0])) {
        let orden = orderDB.documents[0];
        obj.numero_orden = orden.num_orden;
        js.tables.SALES_DOCUMENTS = [];

        // mapeo de los pedidos de la orden
        for (const pedido of orden.pedidos) {
            if (has(pedido, 'numero_pedido') && !isNullOrEmpty(pedido.numero_pedido))
                js.tables.SALES_DOCUMENTS.push({ VBELN: pedido.numero_pedido });
        };

        obj.factura = js;
        return obj;

    } else {
        //lanzar error
        throw new Error(`No se encontro orden para el id_process - ${dt.id_process}`);
    }

}



/**
 * 
 * @param {*} req 
 */
const sapNumeroFactura = async req => {
    const result = new msg.MessageBuilder()
        .setOrigen(`${req.service}/${req.method}`).build();
    let data = {};
    let estado_orden = 'FACTURA_SAP_FALLIDA';
    let numero_orden;
    try {
        req.raiz = "sapNumeroFactura";
        data.request = cloneObject(req);
        data.request.task = "crear-factura-sap";
        data.request.description = "Se crea la factura del pedido en SAP";

        result.process = req.id_process;
        data.id_process = req.id_process;
        data.request.status = "FALLIDO";

        let dta = cloneObject(req);

        //Validacion si viene por flujo 
        if (dta.flow) {
            dta = await getRequestNumFactura(data);
        }
        numero_orden = dta.numero_orden;

        let obj = has(dta, 'factura') ? dta.factura : dta;

        // validacion de formato
        validateSAPNumeroFactura(obj);

        if (gnConfig.logs) console.log("Request RFC SAP >>>:", JSON.stringify(obj));
        // Se ejecuta RFC
        const rfcCrearFactura = await axios({
            headers: { "content-type": "application/json" },
            method: "post",
            url: `${gnConfig.url_rfc_sap}/sap/invoke`,
            data: obj
        });

        const resultRFCCrearFactura = cloneObject(rfcCrearFactura.data);
        result.documents = resultRFCCrearFactura;

        if (gnConfig.logs) console.log("Response RFC SAP >>>:", resultRFCCrearFactura);
        // analiza respuesta RFC - SAP 
        let numeroFacturaSAP;

        if (isArray(resultRFCCrearFactura)) {
            for (let objResult of resultRFCCrearFactura) {
                if (has(objResult, 'ET_SALIDA') && has(objResult.ET_SALIDA, 'FACTURA')) {
                    numeroFacturaSAP = objResult.ET_SALIDA[0].FACTURA;
                    break;
                } else if (has(objResult, 'MESSAGE') && !isNullOrEmpty(objResult.MESSAGE)) {
                    // se generar error
                    numeroFacturaSAP = objResult.MESSAGE_V1;
                    if (numeroFacturaSAP === "") {
                        throw new Error(`No se genero factura en SAP - ${objResult.MESSAGE}`);
                    }

                }
            }

        }


        if (gnConfig.logs) console.log(' Numero de factura SAP >>> ', numeroFacturaSAP);
        if (!isNullOrEmpty(numeroFacturaSAP)) {
            result.status = 200;
            result.success = true;
            result.message = 'Transaction successful';
            result.documents = { numeroFacturaSAP }
            data.request.status = "COMPLETO";
            estado_orden = 'FACTURA_SAP';
            var sapInspira = new SapInspira({
                num_orden: numero_orden,
                // num_pedido:  dta.num_pedido, 
                num_factura: numeroFacturaSAP
            });
            sapInspira.save((err, resul) => {
                if (err) {
                    return console.error(err);
                }
                console.log(" saved to collection.");
            });
        }


    } catch (e) {
        console.log(' Error en sapNumeroFactura >> ', e);
        result = error.errorHandler(e, result);
        data.request.status = "FALLIDO";
    } finally {
        data.response = result;
        trace.runAll(data);
        orderStatus(numero_orden, estado_orden);
        return result;
    }
};

/**
 * 
 * @param {*} params 
 */
async function getOrder(params) {
    try {
        const { data } = await axios.get(`${gnConfig.ruta_orders}/pedido/orden`, { params });
        return data;
    } catch (e) {
        if (gnConfig.logs) console.log("Error en getOrder >>> ", e);
    }
}

/**
 * Se envia la notificación de estados a la orden
 * @param {*} orderNumber numero de orden
 * @param {*} orderStatus estado de la orden
 */
const notifyOrderStatus = async (orderNumber, orderStatus) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (isNullOrEmpty(orderNumber))
                throw new Error("Se requere un numero de orden para actulizar");
            if (isNullOrEmpty(orderStatus))
                throw new Error("Se requiere un estado para actualizar");
            const response = await axios({
                headers: { "content-type": "application/json" },
                method: "put",
                url: `${gnConfig.ruta_orders}/pedido/estado-orden/${orderNumber}`,
                data: { estado: orderStatus }
            });
            resolve(response.data);
        } catch (e) {
            let result = {};
            result.success = false;
            result.message = "Servicio no disponible.";
            result.documents = {
                error: e.message || e.Error || "Servicio no disponible"
            }
            reject(result);
        }
    });
};

/**
 * Funcion para actulizacion de estado de la orden
 * @param { String } orderNumber numero de orden
 * @param { String } status estado que se actulizara
 * @param { String } typeSale tipo de venta de la orden
 * 
 */
function orderStatus(orderNumber, status, typeSale) {
    console.log(`Numero orden [${orderNumber}] - estado [${status}] - tipo venta [${typeSale}]`);

    if (!isNullOrEmpty(typeSale) && typeSale == ValuesVenta.TIPO_VENTA_PREVENTA) {
        updateStatusPresale(orderNumber, { estado: 'Notificacion de factura' })
            .then(r => console.log(" Estado de la orden actualizada presale > ", r))
            .catch(err => console.log(" Error al actualizar estado de la orden presale > ", err))
    } else {
        notifyOrderStatus(orderNumber, status)
            .then(r => console.log(" Estado de la orden actualizada >> ", r))
            .catch(err => console.log(" Error al actualizar estado de la orden > ", err));
    }

}


/**
 * 
 * @param {*} req 
 */
const sapContabilizacion = async req => {
    const result = new msg.MessageBuilder()
        .setOrigen(`${req.service}/${req.method}`).build();
    let data = {};
    let estado_orden = 'CONTABILIZACION_SAP_FALLIDA';

    try {
        console.log(" --- sapContabilizacion >> ", req);

        req.raiz = "sapContabilizacion";
        data.request = cloneObject(req);
        data.request.task = "contabilizacion-sap";
        data.request.description = "contabilizacion de la orden de pedido";

        result.process = req.id_process;
        data.id_process = req.id_process;
        data.request.status = "FALLIDO";

        let dta = await getRequestContabilizacion(data);
        console.log("*****************************");
        console.log(JSON.stringify(dta));
        console.log("*****************************");
        //Se ejecuta RFC
        const rfcContabilizacion = await axios({
            headers: { "content-type": "application/json" },
            method: "post",
            url: `${gnConfig.url_rfc_sap}/sap/invoke`,
            data: dta
        });

        const resultRfcContabilizacion = rfcContabilizacion.data;
        result.documents = resultRfcContabilizacion;
        console.log("*****************************");
        console.log(resultRfcContabilizacion);
        console.log("*****************************");
        // analiza respuesta RFC - SAP 
        let message = [];
        var status, doc_entrega;
        if (isArray(resultRfcContabilizacion)) {
            for (let i = 0; i < dta.importParameterList.I_PEDIDO_SERIAL.length; i++) {
                if (has(resultRfcContabilizacion[0].E_RETURN_ENTREGA[i], 'MESSAGE_V2') && !isNullOrEmpty(resultRfcContabilizacion[0].E_RETURN_ENTREGA[i])) {
                    message.push(resultRfcContabilizacion[0].E_RETURN_ENTREGA[i].MESSAGE);
                    status = "EXITOSO";
                    doc_entrega = resultRfcContabilizacion[0].E_RETURN_ENTREGA[i].DOC_ENTREGA;
                } else {
                    // se generar error
                    message.push(`No se encuentra informacion en SAP - ${resultRfcContabilizacion[0].E_RETURN_ENTREGA[i].MESSAGE}`);
                    status = "FALLIDO";
                    doc_entrega = null;
                    // throw new Error(`No se encuentra informacion en SAP - ${resultRfcContabilizacion.E_RETURN_ENTREGA[i].MESSAGE}`);
                }

                var sapInspira = new SapInspira({
                    num_orden: data.request.num_orden,
                    num_pedido: data.request.num_pedido,
                    serial: dta.importParameterList.I_PEDIDO_SERIAL[i].SERNR,
                    estado: status,
                    doc_entrega: doc_entrega
                });
                sapInspira.save((err, resul) => {
                    if (err) {
                        return console.error(err);
                    }
                    console.log(" saved to collection.");
                });
            }
            // for (let objResult of resultRfcContabilizacion) {
            //     if (has(objResult.E_RETURN_ENTREGA[1], 'MESSAGE_V2') && !isNullOrEmpty(objResult.E_RETURN_ENTREGA[1])) {
            //         message = objResult.E_RETURN_ENTREGA[1].MESSAGE;
            //     } else {
            //         // se generar error
            //         throw new Error(`No se encuentra informacion en SAP - ${objResult.E_RETURN_ENTREGA[1].MESSAGE}`);
            //     }
            // }
        }

        if (gnConfig.logs) console.log('Mensaje contabilizacion exitoso SAP >>> ', message);
        if (!isNullOrEmpty(message)) {
            result.status = 200;
            result.success = true;
            result.message = 'Transaction successful';
            result.documents = { message }
            data.request.status = "COMPLETO";
            estado_orden = 'CONTABILIZACION_SAP';
        }

    } catch (e) {
        console.log(' Error en sapContabilizacion  >> ', e);
        result = error.errorHandler(e, result);
        data.request.status = "FALLIDO";
    } finally {
        data.response = result;
        // trace.runAll(data);
        // orderStatus(numero_orden, estado_orden);
        return result;
    }
};


/**
 * Funcion para consumo RFC - BAPI para generar la factura
 * @param {*} req id_process de la orden
 * @returns documents [] numero de pedido, numero de factura
 */
const numeroFacturaSapBAPI = async req => {
    const result = new msg.MessageBuilder()
        .setOrigen(`${req.service}/${req.method}`).build();

    let dataCache = {};
    let estado_orden = 'FACTURA_SAP_FALLIDA';
    let numero_orden;

    try {
        req.raiz = "numeroFacturaSapBAPI";
        dataCache.request = cloneObject(req);
        dataCache.request.task = "crear-factura-sap";
        dataCache.request.description = "Se crea la factura del pedido en SAP";

        result.process = req.id_process;
        dataCache.id_process = req.id_process;
        dataCache.request.status = "FALLIDO";

        let dta = cloneObject(req);
        //Validacion si viene por flujo 
        if (dta.flow)
            dta = await getReqFacturaBAPI(dta);

        numero_orden = dta.numero_orden;

        let reqBapi = has(dta, 'factura') ? dta.factura : dta;
        dataCache.request.request_sap = cloneObject(reqBapi);
        deletePasswordCache(dataCache.request.request_sap);

        // validacion de formato
        validateFacturaSapBAPI(reqBapi);

        if (gnConfig.logs) console.log("Request RFC SAP >>>:", JSON.stringify(reqBapi));


        // Se ejecuta RFC
        const { data } = await axios({
            headers: { "content-type": "application/json" },
            method: "post",
            url: `${gnConfig.url_rfc_sap}/sap/invoke`,
            data: reqBapi
        });

        result.documents = data;

        if (gnConfig.logs) console.log("Response RFC SAP >>>:", data);
        // analiza respuesta RFC - SAP 

        if (isArray(data)) {
            for (let objResult of data) {
                if (has(objResult, 'NUMERO_FACTURA')) {
                    result.status = 200;
                    result.success = true;
                    result.message = 'Transaction successful';
                    estado_orden = 'FACTURA_SAP';
                } else if (has(objResult, 'Error')) {
                    result.message = objResult.Error;
                } else {
                    result.message = 'NO se ha generado factura SAP';
                }
            }

        }

    } catch (e) {
        console.log(' Error en sapNumeroFactura >> ', e);
        result = error.errorHandler(e, result);
        dataCache.request.status = "FALLIDO";
    } finally {
        dataCache.response = result;
        trace.runAll(dataCache);
        orderStatus(numero_orden, estado_orden);
        return result;
    }
};

/**
 * Funcion para consumo RFC - BAPI para obtener detalles de la factura
 * @param {*} req Request para consumo de BAPI_BILLINGDOC_GETDETAIL
 * @returns data Detalles de la factura 
 */
 const billingdocGetDetail = async req => {
    const result = new msg.MessageBuilder()
    .setOrigen(`${req.service}/${req.method}`).build();

    try {
        
        console.log("Request BillingdocGetDetail >>>:", JSON.stringify(req));
        result.process = req.id_process;

        const { data } = await axios({
            headers: { "content-type": "application/json" },
            method: "post",
            url: `${gnConfig.url_rfc_sap}/sap/invoke`,
            data: req
        });

        result.documents = data;

        if (isArray(data)) {
            for (let [i, objResult] of data.entries()) {
                if (has(objResult, 'BILLINGDOC')) {
                    if (objResult.BILLINGDOC != "") {
                        result.status = 200;
                        result.success = true;
                        result.message = 'Transaction successful';
                    } else {
                        result.message = 'No se ha encontrado información para este número de factura'
                    }
                } else if (has(objResult, `Error${i+1}`)) {
                    result.message = objResult[`Error${i+1}`];
                }
            }
        }

        console.log(data);

    } catch (e) {
        console.log('Error en BillingdocGetDetail >> ', e.message);
        result = error.errorHandler(e, result);
    } finally {
        return result;
    }

};

/**
 * Eliminar contrasena en el consumo de RFC para guardar en cache
 * @param {*} ReqSAP 
 */
function deletePasswordCache(ReqSAP) {
    let haveCredential = has(ReqSAP, 'credentials') && has(ReqSAP.credentials, 'password');
    let password = haveCredential ? ReqSAP.credentials.password : "";

    if (password.length > 0)
        password = "*".repeat(password.length);

    if (haveCredential)
        ReqSAP.credentials.password = password;
}


module.exports = {
    sapNumeroFactura,
    getRequestNumFactura,
    getOrder,
    sapContabilizacion,
    numeroFacturaSapBAPI,
    billingdocGetDetail,
    orderStatus,
};
