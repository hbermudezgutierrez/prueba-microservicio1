"use strict";

const services = require("../services");

const express = require("express"),
    request = require("../lib/request"),
    info = require("../../package.json"),
    routes = require("./download"),
    config = require("../config/general.config"),
    msg = require("../class/message"),
    validate = require("../lib/validate"),
    sftpClient = require('../job/middlewares/sftp-client'),
    sendMail = require('../job/middlewares/send-mail'),
    processfilesftp = require('../job/middlewares/processfilesftp'),
    sap = require('./rfc-sap'),
    app = express(),
    facturacionVenta = require('../services/facturacion-venta.service'),
    { schedulerFacturar, jobProcessingInformation } = require('../job/scheduler/scheduler-facturar'),
    errors = require('./errors'),
    { errorHandler } = require('../lib/error');

const service = "factura";
const result = new msg.MessageBuilder().setOrigen(service).build();

app.post(`/${service}/reportmetricas`, (req, res) => {

    processfilesftp.getReportMetricas(req.body.date).then(resp => {
        res.json({
            resp
        })
    })
});

app.get(`/${service}/`, (req, res) => {
    res.json("Welcome to billing");
});

app.get(`/${service}/version`, (req, res) => {
    res.json({
        service: info.name,
        description: info.description,
        version: info.version
    });
});

app.get(`/${service}/:method`, async (req, res) => {
    try {
        let data = request.getReqGenerales(req);
        data.method = req.params.method;
        data.service = service;
        result.origen = `${service}/${data.method}`;

        switch (data.method) {
            case 'errors':
                //validate.errors(data.query);
                await errors.getErrors(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            case "facturacionVenta":
                data.task = "getFacturacionVenta";
                data.description = "Consulta en db-billing en coll_facturacion_venta";
                facturacionVenta.getFacturacionVenta(req)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            case "collServicesErrors":
                data.task = "getCollServicesErros";
                data.description = "Consulta en db-billing en coll_services_errors";
                facturacionVenta.getCollServicesErros(req)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            default:
                error.errorHandler({ message: 'Origen desconocido.' }, result);
                res.json(result);
        }
    } catch (e) {
        error.errorHandler(e, result);
        res.json(result);
    }
});


// method: pdf en paradigma
app.post(`/${service}/:method`, async function (req, res) {
    try {
        let data = JSON.parse(JSON.stringify(req.body));
        data = request.getReqGenerales(data);
        data.method = req.params.method;
        data.service = service;
        result.process = data.id_process;
        result.origen = `${service}/${data.method}`;

        switch (data.method) {
            case "pdf":
                data.task = "billing-obtener-factura";
                data.description = "Se obtiene la factura pdf desde paradigma";
                if (data.flow) {
                    data = await request.getReqPDF(data);
                } else {
                    validate.postParadigma(data);
                }

                routes
                    .orchParadigma(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            case "sap":
                sftpClient.get();
                result.success = true;
                result.process = undefined;
                result.message = "Execution successful";
                result.documents = {
                    description: "Se ha iniciado la extracción de las facturas"
                };
                res.json(result);
                break;
            case "email-masivo":
                sendMail.exec(req.body);
                result.status = 200;
                result.success = true;
                result.process = undefined;
                result.message = "Execution successful";
                result.documents = {
                    description: "Se ha iniciado el envio de correos masivos"
                };
                res.json(result);
                break;
            case "numero-factura":
                data.task = "billing-generar-factura";
                data.description = "Se obtiene el numero de factura generado desde SAP";
                sap.sapNumeroFactura(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            case "sap-bapi":
                data.task = "billing-generar-factura";
                data.description = "Se obtiene el numero de factura generado desde SAP";
                sap.numeroFacturaSapBAPI(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e));
                break;
            case "contabilizacion":
                data.task = "billing-billing-contabilizacion-pedido";
                data.description = "Se obtiene la contabilizacion de la orden de pedido";
                sap.sapContabilizacion(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e))
                break;
            case "getURLPDFDocumentByBill":
                data.task = "billing-billing-contabilizacion-pedido";
                data.description = "Se obtiene la URL del documento";
                validate.validateGetURLPDFDocByBill(data);
                data = await request.getURLPDFDocumentByBill(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e))
                break;
            case "SendByBill":
                data.task = "billing-billing-contabilizacion-pedido";
                data.description = "Se envia la orden del documento";
                validate.validateSendByBill(data);
                data = await request.sendByBill(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e))
                break;
            case "Prueba":
                data.task = "billing-billing-contabilizacion-pedido";
                data.description = "Se envia la orden del documento";
                validate.validateSendByBill(data);
                data = await request.sendByBillPrueba(data)
                    .then(r => res.json(r))
                    .catch(e => res.json(e))
                break;
            case "job-billing":
                schedulerFacturar();
                result.status = 200;
                result.success = true;
                result.process = undefined;
                result.message = "Execution successful";
                result.documents = jobProcessingInformation();
                res.json(result);
                break;
            case "order-billing":
                validate.validateReqOrderBilling(data);
                data.logs_estados = [{ estado: data.estado }];
                await services.saveOrderBilling(data)
                    .then(r => {
                        result.status = 200;
                        result.success = true;
                        result.message = 'Execution successful';
                        result.documents = { numero_orden: r.numero_orden };
                        res.json(result)
                    })
                    .catch(e => {
                        result.documents = undefined;
                        errorHandler(e, result);
                        res.json(resizeBy);
                    });
                break;
            default:
                throw new Error("Request failed with status code 500");
        }
    } catch (e) {
        result.documents = undefined;
        if (config.logs) console.log(">>>", e);
        if (e.name === 'ValidationError')
            result.status = 400;
        else if (e.status)
            result.status = e.status;
        else
            result.status = 500;

        result.success = false;
        result.message = e.message || e.stack;
        return res.json(result);
    }
});

module.exports = app;