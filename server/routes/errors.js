'use strict';

const msg = require('../class/message'),
    services = require('../services/index'),
    error = require('../lib/error');

/**
 * Funcion para consumo de ms-notification /notification/email
 * @param {*} request consumo de ms-notificacion
 * @returns response de consumo de ms-notification - procesado
 */
const getErrors = async (req) => {
    let result = new msg.MessageBuilder()
        .setOrigen(`${req.service}/${req.method}`)
        .setProcess(req.query.id_process || 0)
        .build();

    return new Promise(async (resolve, reject) => {
        await services.findErrors(req.query)
            .then(resDB => {
                result.success = true;
                result.message = 'Execution successful';
                result.status = 200;
                result.documents = resDB;
                result.length = resDB.length;
                return resolve(result);
            })
            .catch(err => {
                result = error.errorHandler(err, result);
                return reject(result);
            });
    });
}


module.exports = { getErrors };