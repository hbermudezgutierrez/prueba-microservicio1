"use strict";

class MessageBuilder {
  constructor() {
    this.success = false;
    this.origen = "";
    this.process = "0";
    this.status = 500;
    this.message = "Failed transaction";
    this.documents = undefined;
  }
  setSuccess(success) {
    this.success = success;
    return this;
  }
  setOrigen(origen) {
    this.origen = origen;
    return this;
  }
  setProcess(process) {
    this.process = process;
    return this;
  }
  setStatus(status) {
    this.status = status;
    return this;
  }
  setMessage(message) {
    this.message = message;
    return this;
  }
  setDocuments(documents) {
    this.documents = documents;
    return this;
  }
  build() {
    return {
      success: this.success,
      origen: this.origen,
      process: this.process,
      status: this.status,
      message:
        this.message ||
        (this.success ? "Execution successful" : "Failed transaction"),
      documents: this.documents
    };
  }
}

module.exports = { MessageBuilder };
