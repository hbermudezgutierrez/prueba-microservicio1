"use strict";

const cron = require('node-cron'),
    sendmail = require('../job/middlewares/send-mail'),
    processfilesftp = require('../job/middlewares/processfilesftp'),
    metricasConfig = require('../config/reportmetricas.config'),
    sftpClient = require('../job/middlewares/sftp-client'),
    relaunch = require("../job/cron-jobs/relauncha.failed"),
    config = require('../config/general.config'),
    fechas = require('../lib/date'),
    constantes = require('../constants/estado-pedido.constants');

metricasConfig.job_cron_report_metricas


class Schedule {
    constructor() {
        this.cron = "00 06 * * *";
    }
    setCron(cron) {
        this.cron = cron;
        return this;
    }
    run() {
        try {
            cron.schedule(this.cron, function () {
                console.log(`Running a task at  run`, new Date());
                sftpClient.get();
            });
        } catch (e) {
            console.log("e:", e);
        }
    }
    runMail() {
        try {
            cron.schedule(this.cron, function () {
                console.log(` > Running a task at runMail`, new Date());
                let fecha = fechas.formatToYYYYMMDD(fechas.DateAdd('d', -config.dias_consulta_pedidos));
                let date = { fecha };
                setImmediate(() => sendmail.exec(date));
                setTimeout(() => {
                    console.log(' > Re Running a task at runMail', new Date());
                    sendmail.exec(date)
                }, fechas.minutesTomillis(config.tiempo_espera_reenvio_email));
            });
        } catch (e) {
            console.log("e:", e);
        }
    }
    getReportMetricas() {
        try {
            cron.schedule(this.cron, function () {
                console.log(`Running a task at  getReportMetricas`, new Date());
                processfilesftp.getReportMetricas();
            });
        } catch (e) {
            console.log("e:", e);
        }
    }
    relaunchFailedCron() {
        try {
            cron.schedule(this.cron, function () {
                console.log(`Running a task at  relaunchFailed`, new Date());

                relaunch.relaunchFailed();
            });
        } catch (e) {
            console.log("e:", e);
        }
    }
}

module.exports = new Schedule();