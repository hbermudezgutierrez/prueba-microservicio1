"use strict";

const mongoose = require("mongoose"),
    dbconfig = require("../config/database.config");

class Database {
  constructor() {
    this._connect()
  }
  
  _connect() {
       mongoose.connect(dbconfig.urldb, { useNewUrlParser: true })
         .then(() => {
           console.log('Database <Billing> connection successful: ONLINE')
         })
         .catch(err => {
           console.error("Database connection ERROR in URLDB:", `mongodb://${dbconfig.urldb.split("@")[1]}`);
         })
    }
}

module.exports = new Database();