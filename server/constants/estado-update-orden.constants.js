/**
 * Estados de actualizaciones para la orden
 */

module.exports = {

    CONTABILIZACION_OK: 'CONTABILIZACION_SAP',
    CONTABILIZACION_ERR: 'CONTABILIZACION_SAP_FALLIDA',
};