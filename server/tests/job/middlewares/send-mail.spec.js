const sinon = require('sinon');
const { describe, it } = require('mocha');
const { expect } = require('chai');
const sendMail = require('../../../job/middlewares/send-mail');
const getOrderDB = require('../../../services/index');
const notification  = require('../../../job/cron-jobs/services/notification');
const service = require('../../../services/index');
const error = require("../../../lib/error");
const download = require("../../../routes/download");

describe("Test 'exec' method of 'send-mail'", () => {

    let stubGetOrderDB, stubSendMail, stubUpdate, stubURL;

    let responseURL = {
        documents: {
            url: "https://facturasclaro.paradigma.com.co/ebpClaro//Download//PDF//2020-11-05-15-05_bc22e1aa-cfae-4ef8-bb71-5158461fe330.pdf"
        }
    }

    beforeEach(() => {

        stubGetOrderDB = sinon.stub(getOrderDB, 'getOrdersDB')
            .returns(getOrderRes);

        stubSendMail = sinon.stub(notification, 'sendEmail');

        stubUpdate = sinon.stub(service, 'updatePedidos')
            .returns(true);

        stubURL = sinon.stub(download, 'orchParadigma')
            .resolves(responseURL);
        
    })

    afterEach(() => {
        stubGetOrderDB.restore();
        stubSendMail.restore();
        stubUpdate.restore();
        stubURL.restore();
    })

    let getOrderRes = {
        documents: [
            {
                _id: '5f71f5277b352647343f7e5b',
                id_process: '5f5a8837838b59624627d0ff',
                cabecera : {
                    "num_doc_cliente" : "1020443235",
                    "nombre_cliente" : "JIMENEZ PAMPLONA, ANGELA MARIA",
                    "ciudad" : "MEDELLIN",
                    "telef_movil" : "3116090964",
                    "telef_adicional" : "",
                    "email" : "javerhitss@gmail.com",
                    "direccion" : "Calle 6C Sur 84A 35 Apto 1006 Rodeo Alto "
                },
                numero_orden: '5249648758',
                numero_pedido: '1067320671',
                tipo_pedido: 'Tangible',
                tip_venta: 'Prepago',
                proceso_venta: 'ACT',
                estado_pedido: 'FACTURADO',
                numero_factura: '3107196172',
                url_factura: 'https://facturasclaro.paradigma.com.co/ebpClaro//Download//PDF//2020-11-05-15-05_bc22e1aa-cfae-4ef8-bb71-5158461fe330.pdf'
            },
            {
                _id: '5f71f5047b352647343f7e4a',
                id_process: '5f5a8837838b59624627d0ff',
                cabecera : {
                    "num_doc_cliente" : "1020443235",
                    "nombre_cliente" : "JIMENEZ PAMPLONA, ANGELA MARIA",
                    "ciudad" : "MEDELLIN",
                    "telef_movil" : "3116090964",
                    "email" : "javerhitss@gmail.com",
                    "direccion" : "Calle 6C Sur 84A 35 Apto 1006 Rodeo Alto "
                },
                numero_orden: '5249648759',
                numero_pedido: '1067582322',
                tipo_pedido: 'Intangible',
                tip_venta: 'Prepago',
                proceso_venta: 'ACT',
                estado_pedido: 'FACTURADO1',
                numero_factura: '3107196169',
                url_factura: 'https://facturasclaro.paradigma.com.co/ebpClaro//Download//PDF//2021-06-15-08-46_ec877a4e-7839-475c-b094-93bc08086bea.pdf'
            },
            {
                _id: '5f71f5277b352647343f7e4b',
                id_process: '5f5a8837838b59624627d0ff',
                cabecera : {
                    "num_doc_cliente" : "1020443235",
                    "nombre_cliente" : "JIMENEZ PAMPLONA, ANGELA MARIA",
                    "ciudad" : "MEDELLIN",
                    "telef_movil" : "3116090964",
                    "telef_adicional" : "",
                    "email" : "javerhitss@gmail.com",
                    "direccion" : "Calle 6C Sur 84A 35 Apto 1006 Rodeo Alto "
                },
                numero_orden: '5249648759',
                numero_pedido: '1067320670',
                tipo_pedido: 'Tangible',
                tip_venta: 'Prepago',
                proceso_venta: 'ACT',
                estado_pedido: 'FACTURADO',
                numero_factura: '3107196172',
                url_factura: 'https://facturasclaro.paradigma.com.co/ebpClaro//Download//PDF//2021-06-15-08-46_ec877a4e-7839-475c-b094-93bc08086bea.pdf'
            }
        ]
    };

    it('Send Mail', async () => {

        let payload = {
            fecha: '2021-01-01'
        };

        stubSendMail.resolves({sucess: true});

        const value = await sendMail.exec(payload);
        expect(value).to.not.be.undefined;
        sinon.assert.called(stubGetOrderDB);
        sinon.assert.called(stubSendMail);
        sinon.assert.called(stubUpdate);
        sinon.assert.called(stubURL);
    });

    it('Risk validation Catch Error', async () => {

        stubSendMail.rejects();
        sinon.stub(error, 'errorHandler');

        const value = await sendMail.exec();
        expect(value).to.be.undefined;
    });
});