"use strict";

const express = require('express'),
    chai = require('chai'),
    chaiHttp = require('chai-http'),
    expect = require('chai').expect,
    app = express();


chai.use(chaiHttp);

/**
 * Funcion que realiza la configuracion inicial para levantar el server
 * @returns 
 */
function createApp() {
    app.use(express.urlencoded({ extended: false }))
        .use(express.json())
        .use(require("../routes/index"));

    return app;
}

/**
 * Funcion para ejecuccion test unitario del POST para consumo del 
 * path /factura/sap-bapi
 */
describe(' --- Consulta de numero de factura SAP - BAPI --- ', () => {

    before(function (Done) {
        let app = createApp();
        app.listen(function (err) {
            if (err) { return Done(err) }
            Done();
        });
    });


    it(' ** Consulta numero de factura SAP - BAPI  - POST ** ', (done) => {
        chai.request(app)
            .post('/factura/sap-bapi')
            .type('json')
            .send({
                flow: true,
                id_process: "6075c7ee05c7668662ecb45d"
            })
            .end(function (err, res) {
                console.log(" -- Response test Factura SAP -- ", res.body);
                expect(res).to.have.status(200);
                expect(res.body).to.be.an('Object');
                expect(res.body).to.include.keys('message', 'documents');
                expect(res.body).that.have.nested.property('message')
                    .that.is.an('string').equal('Transaction successful');
                expect(res.body.documents).to.be.an('array').that.have.deep.members([{
                    NUMERO_FACTURA: "3109100750",
                    NUMERO_PEDIDO: "1071150207"
                }]);
                done();
            });
    });

})

