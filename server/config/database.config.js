"use strict";

// traza de kafka
process.env.COLLECTION_CONSUMER || "coll_ms_billing";

process.env.MONGO_URL_KAFKA_CONSUMER || "mongodb+srv://ecommerce:i3PHyhLRcgnIOcJG@clu-mongo-ecom-dev01-pri.qremx.mongodb.net/db_consumer";


module.exports = {
    urldb: process.env.DB_URI || "mongodb+srv://ecommerce:i3PHyhLRcgnIOcJG@clu-mongo-ecom-dev01-pri.qremx.mongodb.net/db_billing",

    orders_collection: process.env.ORDERS_COLLECTION || 'coll_orders',

    files_collection: process.env.FILES_COLLECTION || 'coll_files',

    sap_inspira_collection: process.env.SAP_INSPIRA_COLLECTION || 'coll_sap_inspira',

    facturacion_venta_collection: process.env.FACTURACION_VENTA_COLLECTION || 'coll_facturacion_venta',

    services_errors_collection: process.env.SERVICES_ERRORS_COLLECTION || 'coll_services_errors',

    order_collection: 'coll_orders_billing',

    file_sftp_collection: 'coll_files_sftp',

    service_errors_collection: 'service_errors'
}