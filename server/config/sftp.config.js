"use strict";

module.exports = {
  sftp_host: process.env.SFTP_HOST || '172.22.94.68',
  
  sftp_port: process.env.SFTP_PORT || '22',
  
  sftp_user: process.env.SFTP_USER || 'digital100sftp',
  
  sftp_pass: process.env.SFTP_PASS || 'V3nD1g1$19&',

  file_start_name: process.env.FILE_START_NAME || 'ZSDCOM_',
  
  file_remote_path: process.env.FILE_REMOTE_PATH || '/home/digital100/pruebas/logs/sap/spool_ventas'
}