"use strict";

process.env.NODE_ENV = process.env.NODE_ENV || "dev";

process.env.TZ = 'America/Bogota';

process.env.MONGO_URL = process.env.MONGO_URL || "mongodb+srv://ecommerce:i3PHyhLRcgnIOcJG@clu-mongo-ecom-dev01-pri.qremx.mongodb.net/db_postpago";

process.env.KAFKA_CLIENT_ID || "kafka";

process.env.KAFKA_BROKERS || "10.17.0.9:30495";

module.exports = {
  logs: process.env.LOGS || true,

  port: process.env.PORT || 3002,

  ruta_comunes: process.env.RUTA_COMUNES || "http://ms-common-orquestadordev.apps.r05oof71.eastus2.aroapp.io",

  url_paradigma:
    process.env.URL_PARADIGMA ||
    "https://facturasclaro.paradigma.com.co/ebpClaro/Pages/Services/ServiceAppClaro.aspx/getURLPDFDocumentByBillNumber",

  url_paradigma2:
    process.env.URL_PARADIGMA2 ||
    "http://facturasclarocert.paradigma.com.co/ebpTelmexMain/Pages/Services/ServiceAppClaro.aspx/getURLPDFDocumentByBillNumberV2",

  encryption_key: process.env.ENCRYPTION_KEY || 'A9b8C7d6E5f4G3h2',

  encryption_key_v2: process.env.ENCRYPTION_KEY_V2 || '8F09D05756DBEA2B',

  job_cron_expression: process.env.JOB_CRON_EXPRESSION || '30 53 12 * * *',

  reporte_ventas_separador: process.env.REPORTE_VENTAS_SEPARADOR || '§¶',

  usuario_orquestador_sap: process.env.USUARIO_ORQUESTADOR_SAP || 'CO_WDIGITAL',

  ruta_orders: process.env.RUTA_ORDERS || "http://ms-orders-orquestadordev.apps.r05oof71.eastus2.aroapp.io",

  limite_pedidos: process.env.LIMITE_PEDIDOS || 200,

  mail_subject: process.env.MAIL_SUBJECT || 'Factura Tienda Virtual Claro',

  mail_displayname: process.env.MAIL_DISPLAYNAME || 'Notificación Tienda Virtual Claro',

  mail_contentType: process.env.CONTENT_TYPE || 'MESSAGE',

  mail_plantillaSimPaqueteId: process.env.PLANTILLA_SIM_PAQUETE_ID || 'PREPAGO',

  estado_pedido_email: process.env.ESTADO_PEDIDO_EMAIL || "FACTURADO",

  job_cronMail_expression: process.env.JOB_CRONMAIL_EXPRESSION || '30 30 30 * *',

  url_rfc_sap: process.env.URL_RFC_SAP || 'http://rfc-sap-orquestadordev.apps.r05oof71.eastus2.aroapp.io',

  user_sap_inspira: process.env.USER_SAP || "co_pruebas",

  pass_sap_inspira: process.env.PASS_SAP || "Bogota.16*",

  dias_maximo_validacion_archivos: process.env.DIAS_MAXIMOS_VALIDACION_ARCHIVOS || '70',

  estado_error: process.env.ESTADO_ERROR || 'ERROR',

  estado_sap: process.env.ESTADO_SAP || 'EN PROCESO',

  estado_generarURL: process.env.ESTADO_URL || 'GENERADO',

  estado_notificado: process.env.ESTADO_NOTIFICADO || 'NOTIFICADO',

  canal_factura_venta: process.env.CANAL_FACTURA_VENTA || 'ECOM',

  mensaje_canal_invalido: process.env.MENSAJE_CANAL_INVALIDO || 'El canal ingresado no es valido',

  ruta_notifications: process.env.RUTA_NOTIFICATION || 'http://ms-notification-orquestadordev.apps.r05oof71.eastus2.aroapp.io',

  tipo_cliente_consumo_paradigma: process.env.TIPO_CLIENTE_CONSUMO_PARADIGMA || 'S',

  cron_expression_job_facturacion: process.env.CRON_EXPRESSION_JOB_FACTURACION || '0 */30 * * * *',

  tiempo_espera_envio_email: process.env.TIEMPO_ESPERA_ENVIO_EMAIL || '5',

  tiempo_espera_reenvio_email: process.env.TIEMPO_ESPERA_REENVIO_EMAIL || '20',

  cron_expression_relaunchFailed: process.env.CRON_EXPRESSION_RELAUNCH_FAILED || '*/60 * * * *',

  email_content_type: 'CONTENTID',

  email_template_orden_entregado_cliente: process.env.EMAIL_TEMPLATE_ORDEN_ENTREGADO_CLIENTE || 'orderentregadocliente',

  dias_consulta_pedidos: process.env.DIAS_CONSULTA_PEDIDOS || 30,

  ruta_presale: process.env.RUTA_PRESALE || 'http://ms-presale-orquestadordev.apps.r05oof71.eastus2.aroapp.io',

  ruta_rfc_sap: process.env.RUTA_RFC_SAP || 'http://rfc-sap-orquestadordev.apps.r05oof71.eastus2.aroapp.io',

};
