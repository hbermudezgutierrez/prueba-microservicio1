'use strinc';

const info = require("../../package.json");

module.exports = {

  nats_cluster_id: process.env.NATS_CLUSTER_ID || 'test-cluster',

  nats_servers: process.env.NATS_SERVERS || 'nats://localhost:4222',
  
  consumer_group: info.name

}
