'use strinc';

process.env.ELASTIC_APM_SERVICE_NAME = process.env.ELASTIC_APM_SERVICE_NAME || "ms-billing";
process.env.ELASTIC_APM_SERVER_URL = process.env.ELASTIC_APM_SERVER_URL || "http://100.123.251.89:8200";
process.env.ELASTIC_APM_ENVIRONMENT = process.env.ELASTIC_APM_ENVIRONMENT || "100%DigitalDev";