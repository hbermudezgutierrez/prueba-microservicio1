module.exports = {

    mascara_archivo: process.env.MASCARA_ARCHIVO || 'COMetric_',
    path_orquestador: process.env.PATH_ORQUESTADOR || '/home/digital100/pruebas/logs/oms/ventas',
    output_route_oms: process.env.OUTPUT_ROUTE_OMS || '/home/digital100/pruebas/logs/oms/ventas/reportes_prod',
    output_route_proccess_oms: process.env.OUTPUT_ROUTE_PROCCESS_OMS || '/home/digital100/pruebas/logs/oms/ventas/procesados',
    code_npedidos: process.env.CODE_NPEDIDOS || '1013',
    code_sumatoria: process.env.CODE_SUMATORIA || '1012',
    code_ordenes: process.env.CODE_ORDENES || '1016',
    mascara_archivo_oms: process.env.MASCARA_ARCHIVO_OMS || 'OMS_VENTAS_DIGITAL100_',
    area_oms: process.env.AREA_OMS || 'OMS',
    secuence_sum_oms: process.env.SEQUENCE_SUM_OMS || '001',
    sequence_count_oms: process.env.SEQUENCE_COUNT_OMS || '002',
    sequence_count_ordenes_oms: process.env.SEQUENCE_COUNT_ORDENES_OMS || '003',

    path_sap: process.env.PATH_SAP || '/home/digital100/pruebas/logs/sap/spool_ventas',
    output_route_sap: process.env.OUTPUT_ROUTE_SAP || '/home/digital100/pruebas/logs/sap/spool_ventas/reportes_prod',
    output_route_proccess_sap: process.env.OUTPUT_ROUTE_PROCCESS_SAP || '/home/digital100/pruebas/logs/sap/spool_ventas/procesados',

    code_suma_pedidos: process.env.CODE_SUMA_PEDIDOS || '1010',
    code_conteoPedidos: process.env.CODE_CONTEO_PEDIDOS || '1011',
    usuario_sap: process.env.USUARIO_SAP || 'CO_WDIGITAL',
    mascara_archivo_sap: process.env.MASCARA_ARCHIVO_SAP || 'ZSDCOM_',
    area_sap: process.env.AREA_SAP || 'SAP',
    sequence_sum_sap: process.env.SEQUENCE_SUM_SAP || '001',
    secuence_count_sap: process.env.SEQUENCE_COUNT_SAP || '002',

    filtro1_sap_ncampo: process.env.FILTRO1_SAP_NCAMPO || '18',
    valor_filtro_motivo_pedido: process.env.MOTIVO_PEDIDO || 'C08',
    valor_filtro_motivo_pedido_ncampo:process.env.MOTIVO_PEDIDO_NCAMPO || "55",
    job_cron_report_metricas: process.env.JOB_CRON_REPORTE_METRICAS || '15 * * * * *'






};