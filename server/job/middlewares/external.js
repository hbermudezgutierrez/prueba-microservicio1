"use strict";

const axios = require("axios"),
  gnConfig = require("../../config/general.config"),
  cypher = require("../../lib/cypher");

const getURL = async req => {
    let result = "";
  try {
    const token = cypher.encrypt(`${req.invoice}|S`);
    
    let re = await axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: gnConfig.url_paradigma,
      preambleCRLF: true,
      postambleCRLF: true,
      data: JSON.stringify({data: token})
    });

    let dataResp = JSON.parse(re.data.d);

    if (dataResp.error.isError === true) {      
      throw new Error(JSON.parse(re.data.d).error.msg);
    }
    
    if (dataResp.url !== ""){
      result = JSON.parse(re.data.d).url;
    }
  } catch (e) {
    if (gnConfig.logs) console.log("Error >>>:", e);
  } finally{
    return result;
  }
};

const putCache = async req => {
  let result = "";
try {
  console.log("req en putCache:", req)
  let re = await axios({
    headers: { "content-type": "application/json" },
    method: "post",
    url: `${gnConfig.ruta_comunes}/comunes/cache/masivo`,
    preambleCRLF: true,
    postambleCRLF: true,
    data: req
  });

  console.log("re:", re);

  // if (JSON.parse(re).error.isError === true) {      
  //   throw new Error(JSON.parse(re.data.d).error.msg);
  // }
  
  // if (JSON.parse(re.data.d).url !== ""){
  //   result = JSON.parse(re.data.d).url;
  // }
} catch (e) {
  if (gnConfig.logs) console.log("Error >>>:", e);
} finally{
  return result;
}
};

const updatePedidos = async req => {
  let result = "";
try {  
  console.log("req en updatePedidos:", req)
  let re = await axios({
    headers: { "content-type": "application/json" },
    method: "post",
    url: `${gnConfig.ruta_orders}/pedido/sap-masivo`,
    preambleCRLF: true,
    postambleCRLF: true,
    data: req
  });

  console.log("re:", re);
} catch (e) {
  if (gnConfig.logs) console.log("Error >>>:", e);
} finally{
  return result;
}
};

module.exports = { getURL, putCache, updatePedidos };
