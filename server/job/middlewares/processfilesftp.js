"use strict";

const dateformat = require('dateformat');
const utilities = require('../../lib/utilities'),


    Files = require('../models/files.model'),
    ftConfig = require('../../config/sftp.config'),
    metricasConfig = require('../../config/reportmetricas.config'),

    Client = require('ssh2-sftp-client')

function moveFileFTP(oldPath, newPath, element_name) {
    const conn = {
        host: ftConfig.sftp_host,
        port: ftConfig.sftp_port,
        username: ftConfig.sftp_user,
        password: ftConfig.sftp_pass
    };
    let remoteOldPath = `${oldPath}/${element_name}`;
    let remoteNewPath = `${newPath}/${element_name}`;
    let sftp = new Client()
    sftp.connect(conn).then(() => {
        console.log("Se realizo correctamente la conexion a la FTP  ");

        return sftp.rename(remoteOldPath, remoteNewPath, err => {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy();
                } else { callback(err); }
                return;
            }
            callback();
        });

        function copy() {
            var readStream = sftp.createReadStream(remoteOldPath);
            var writeStream = sftp.createWriteStream(remoteNewPath);
            readStream.on('error', callback);
            writeStream.on('error', callback);
            readStream.on('close', function () { sftp.unlink(remoteOldPath, callback); });
            readStream.pipe(writeStream);
        }


    }).catch(reason => {
        console.log("No se pudo conectar a la FTP  ");
        console.error(reason);

    });
}


//data datos del archivo
//sftp_upload_path: ruta donde se colocara el archivo
//element_name: nombre del archivo con la extension ejm archivo.txt
function uploadOrdersFile(data, sftp_upload_path, element_name) {

    const conn = {
        host: ftConfig.sftp_host,
        port: ftConfig.sftp_port,
        username: ftConfig.sftp_user,
        password: ftConfig.sftp_pass
    };
    let remoteFileName = `${sftp_upload_path}/${element_name}`;
    let sftp = new Client()
    sftp.connect(conn).then(() => {
        console.log("Se realizo correctamente la conexion a la FTP  ");
        let buf = Buffer.from(data, 'utf8');
        return sftp.put(buf, remoteFileName);
    }).catch(reason => {
        console.log("No se pudo conectar a la FTP  ");
        console.error(reason);

    });

}

//Esta funcion nos devuelve la data de un archivo encontrado en 
//la FTP
//file_remote_path: ruta donde se encuentra el archivo
//element_name: nombre del archivo
function getFileFTP(file_remote_path, element_name) {
    return new Promise((resolve, reject) => {
        const conn = {
            host: ftConfig.sftp_host,
            port: ftConfig.sftp_port,
            username: ftConfig.sftp_user,
            password: ftConfig.sftp_pass
        };
        let sftp = new Client();

        sftp.connect(conn).then(() => {
            console.log("Se realizo correctamente la conexion a la FTP  ");
            return sftp.get(`${file_remote_path}/${element_name}`).then(result => {
                resolve(result);
            })

        }).catch(reason => {
            console.log("No se pudo conectar a la FTP  ");
            console.error(reason);
            reject(reason);
        });
    });
}


//Este medodo se usa para buscar un archivo en la FTP enviando 
//parte del nombre o el nombre completo en el campo element_name
// y nos devuelve el nombre completo
function searchFile_FTP(file_remote_path, element_name, extension) {
    return new Promise((resolve, reject) => {
        const conn = {
            host: ftConfig.sftp_host,
            port: ftConfig.sftp_port,
            username: ftConfig.sftp_user,
            password: ftConfig.sftp_pass
        };
        let sftp = new Client();
        let isData = false;
        sftp.connect(conn).then(() => {
            console.log("Se realizo correctamente la conexion a la FTP  ");
            sftp.list(file_remote_path).then(result => {

                if (!utilities.isNullOrEmpty(result[0])) {
                    for (var i = 0; i < result.length; i++) {

                        if (result[i].name.toLowerCase().includes(element_name.toLowerCase()) && result[i].name.toLowerCase().includes(extension.toLowerCase())) {
                            console.log(result[i].name);
                            isData = true;
                            resolve(result[i].name)
                        }

                        if (i == result.length - 1) {
                            if (!isData) {
                                console.log('No se encontro el archivo en la FTP');
                                resolve(null);
                            }
                        }
                    }
                } else {
                    console.log('No se encontraron archivos en la ftp');
                    resolve(null);
                }
            })

        }).catch(reason => {
            console.log("No se pudo conectar a la FTP  ");
            console.error(reason);
            reject(reason);
        });

    });
}

// Este metodo se usa para contar sumar y contar sin tener en cuenta los repetido
//data informacion del archivo
//fieldName: Nombre del campo que desea contar o sumar o el numero de la colunna
//separator: caracter que separa los datos
//Option campo que indica uno de los siguientes procesos
// option: 1 para contar todo
// option: 2 sumar
// option:3 contar sin tener encueta los repetidos
function countData(data, fieldName, separator, option) {
    return new Promise((resolve, reject) => {
        let rows = data.split('\n');
        let row1 = rows[0].split(separator);
        let fieldNumber;
        let row2;
        let nField = 0;
        let arrayDdata = [];
        let existData = false;

        for (var i = 0; i < row1.length; i++) {
            if (row1[i] === fieldName || !isNaN(fieldName)) {
                if (isNaN(fieldName)) {
                    fieldNumber = i;
                } else {

                    fieldNumber = Number(fieldName)
                }


                for (var j = 1; j < rows.length; j++) {

                    row2 = rows[j].split(separator);
                    if (!utilities.isNullOrEmpty(row2[i])) {

                        switch (option) {

                            case 1:
                                {
                                    nField = Number(nField) + 1
                                    break
                                }
                            case 2:
                                {
                                    nField = Number(nField) + Number(row2[i]);
                                    break;
                                }
                            case 3:
                                {
                                    existData = false;
                                    for (var ii = 0; ii < arrayDdata.length; ii++) {
                                        if (arrayDdata[ii] == row2[i]) {
                                            existData = true;
                                            break;
                                        }

                                    }
                                    if (!existData) {
                                        arrayDdata.push(row2[i]);
                                    }

                                    break;

                                }

                            default:
                                reject("error:Ingrese un de las 3 opciones")
                                break;

                        }

                    }

                }

            }
        }
        if (option == 3) {
            nField = arrayDdata.length;
        }
        // console.log(arrayDdata);
        console.log(nField);
        resolve(nField);

    });
}



// Este metodo se usa para contar sumar y contar sin tener en cuenta los repetido
//data informacion del archivo
//fieldName: numero de la columna del campo que desea contar o sumar 
//filterFieldNumber: numero de la columna del filtro
//nameFilter: nombre del filtro o filtro
//separator: caracter que separa los datos
//Option campo que indica uno de los siguientes procesos
// option: 1 para contar todo
// option: 2 sumar
// option:3 contar sin tener encueta los repetidos
function countDataFilter(data, numberField, separator, option, filterFieldNumber, nameFilter, filterFieldNumber4, nameFilter4) {
    return new Promise(async (resolve, reject) => {
        let rows = data.split('\n');
        let row2;
        let nField = 0;
        let arrayDdata = [];
        let existData = false;
        console.log("colunnas  " + rows.length);
        try {


            console.log(filterFieldNumber);
            for (var j = 0; j < rows.length; j++) {

                row2 = await rows[j].split(separator);

                if (!utilities.isNullOrEmpty(row2[filterFieldNumber])) {

                    switch (option) {

                        case 1:
                            {

                                if ((utilities.isNullOrEmpty(filterFieldNumber) || row2[filterFieldNumber].includes(nameFilter)) && (!utilities.isNullOrEmpty(row2[numberField])) && (utilities.isNullOrEmpty(filterFieldNumber4) || row2[filterFieldNumber4].includes(nameFilter4))) {
                                    nField = Number(nField) + 1
                                }
                                console.log("numero de colunna usuario " + row2[filterFieldNumber] + " motivo pedido  " + row2[filterFieldNumber4] + "filtro  " + filterFieldNumber4);
                                console.log("filtro usuario "+ (utilities.isNullOrEmpty(filterFieldNumber) || row2[filterFieldNumber].includes(nameFilter)));
                                console.log("numero de campo " + (!utilities.isNullOrEmpty(row2[numberField])));
                                console.log("motivo pedido" + (utilities.isNullOrEmpty(filterFieldNumber4) || row2[filterFieldNumber4].includes(nameFilter4)));
                                break
                            }
                        case 2:
                            {
                                if ((utilities.isNullOrEmpty(filterFieldNumber) || row2[filterFieldNumber].includes(nameFilter)) && (!utilities.isNullOrEmpty(row2[numberField])) && (utilities.isNullOrEmpty(filterFieldNumber4) || row2[filterFieldNumber4].includes(nameFilter4))) {

                                    nField = Number(nField) + Number(row2[numberField]) * 100;
                                }

                                break;
                            }
                        case 3:
                            {
                                if ((utilities.isNullOrEmpty(filterFieldNumber) || row2[filterFieldNumber].includes(nameFilter))(!utilities.isNullOrEmpty(row2[numberField])) && (utilities.isNullOrEmpty(filterFieldNumber4) || row2[filterFieldNumber4].includes(nameFilter4))) {

                                    existData = false;
                                    for (var ii = 0; ii < arrayDdata.length; ii++) {
                                        if (arrayDdata[ii] == row2[i]) {
                                            existData = true;
                                            break;
                                        }

                                    }
                                    if (!existData) {
                                        arrayDdata.push(row2[i]);
                                    }

                                    break;

                                }
                            }
                        default:
                            reject("error:Ingrese un de las 3 opciones")
                            break;

                    }

                }

            }

            if (option == 3) {
                nField = arrayDdata.length;
            }
            // console.log(arrayDdata);
            console.log("contador " + nField);
            resolve(nField);
        } catch (error) {
            console.log(error);
        }
    });
}

//Funcion que genera los reportes de metrica

function getReportMetricas(date) {
    return new Promise(async (resolve, reject) => {
        let dateYesterdaySring;
        let dateWithoutMinutes;


        let dateReport;
        let startDate;

        let npedidos = 0;
        let sumatoria = 0;
        let ordenes = 0;
        if (utilities.isNullOrEmpty(date)) {
            startDate = new Date();
            console.log(" data  aarra" + startDate);
        } else {
            let startDateLocal = new Date();
            startDate = new Date(date + " " + startDateLocal.getHours() + ':' + startDateLocal.getMinutes() + ":00");

        }
        console.log(startDate);
        getFormatDate(startDate, 1).then(result => {
            dateReport = result;
        })

        let dateYesterday = new Date(startDate.getTime() - (3600000 * 24));
        console.log(dateYesterday);
        await getFormatDate(dateYesterday, 2).then(result => {
            dateYesterdaySring = result;
            console.log(dateYesterdaySring);
        })


        await getFormatDate(startDate, 2).then(result => {
            dateWithoutMinutes = result;
            console.log(dateWithoutMinutes);
        })

        console.log(dateYesterdaySring);
        console.log(dateWithoutMinutes);
        console.log(metricasConfig.path_orquestador + ' ' + metricasConfig.mascara_archivo_oms + dateWithoutMinutes);
        await searchFile_FTP(metricasConfig.path_orquestador, metricasConfig.mascara_archivo_oms + dateWithoutMinutes, 'txt').then(async data => {

            if (!utilities.isNullOrEmpty(data)) {
                console.log(data);
                await getFileFTP(metricasConfig.path_orquestador, data).then(async result => {

                    await countData(result + "", 'NUMPEDIDO', '|', 1).then(results => {
                        npedidos = results;
                        console.log("npedidos " + npedidos);
                    });
                    await countData(result + "", 'VALORIVA', '|', 2).then(results => {
                        sumatoria = results;
                        console.log("sumatoria " + sumatoria);
                    });
                    await countData(result + "", 'NUMORDEN', '|', 3).then(results => {
                        ordenes = results;
                        console.log("ordenes " + ordenes);
                    });
                    moveFileFTP(metricasConfig.path_orquestador, metricasConfig.output_route_proccess_oms, data);
                })

            }
        });

        let reporte1 = metricasConfig.code_npedidos + '|' + npedidos + '|2|0|0|0|1|' + dateYesterdaySring;
        let reporte2 = metricasConfig.code_sumatoria + '|' + sumatoria + '|2|0|0|0|1|' + dateYesterdaySring;
        let reporte3 = metricasConfig.code_ordenes + '|' + ordenes + '|2|0|0|0|1|' + dateYesterdaySring;



        uploadOrdersFile(reporte1, metricasConfig.output_route_oms, metricasConfig.mascara_archivo + metricasConfig.area_oms + '_' + metricasConfig.sequence_count_oms + '_' + dateReport + '.txt');

        uploadOrdersFile(reporte2, metricasConfig.output_route_oms, metricasConfig.mascara_archivo + metricasConfig.area_oms + '_' + metricasConfig.secuence_sum_oms + '_' + dateReport + '.txt');

        uploadOrdersFile(reporte3, metricasConfig.output_route_oms, metricasConfig.mascara_archivo + metricasConfig.area_oms + '_' + metricasConfig.sequence_count_ordenes_oms + '_' + dateReport + '.txt');


        let sumaPedidos = 0
        let conteoPedidos = 0;

        await searchFile_FTP(metricasConfig.path_sap, metricasConfig.mascara_archivo_sap + dateWithoutMinutes, 'txt').then(async file => {

            if (!utilities.isNullOrEmpty(file)) {

                await getFileFTP(metricasConfig.path_sap, file).then(async result => {
                    let data = result + '';
                    data = data.replace(/[¶]/g, '').replace(/['Â]/g, '').replace(/[§]/g, '|');


                    await countDataFilter(data + "", 34, '|', 2, 46, metricasConfig.usuario_sap,  parseInt(metricasConfig.valor_filtro_motivo_pedido_ncampo, 10), metricasConfig.valor_filtro_motivo_pedido).then(results => {
                        sumaPedidos = results;
                        console.log("sumaPedidos sap " + sumaPedidos);

                    })
                    await countDataFilter(data + "", 48, '|', 1, 46, metricasConfig.usuario_sap,  parseInt(metricasConfig.valor_filtro_motivo_pedido_ncampo, 10), metricasConfig.valor_filtro_motivo_pedido).then(results => {
                        conteoPedidos = results;
                        console.log("conteoPedidos sap " + conteoPedidos);

                    })
                    moveFileFTP(metricasConfig.path_sap, metricasConfig.output_route_proccess_sap, file);
                })
            }
        });


        let repor4 = metricasConfig.code_suma_pedidos + '|' + sumaPedidos + '|2|0|0|0|1|' + dateYesterdaySring;
        let reporte5 = metricasConfig.code_conteoPedidos + '|' + conteoPedidos + '|2|0|0|0|1|' + dateYesterdaySring;
        uploadOrdersFile(repor4, metricasConfig.output_route_sap, metricasConfig.mascara_archivo + metricasConfig.area_sap + '_' + metricasConfig.sequence_sum_sap + '_' + dateReport + '.txt')
        uploadOrdersFile(reporte5, metricasConfig.output_route_sap, metricasConfig.mascara_archivo + metricasConfig.area_sap + '_' + metricasConfig.secuence_count_sap + '_' + dateReport + '.txt')


        let res = {
            ok: true,
            report: "El reporte se ha generado correctamente y subido a servidor"

        };
        resolve(res);




    });

    function getFormatDate(dateToFormat, format) {
        let date;
        let dateReport;
        return new Promise(async (resolve, reject) => {

            if (format == 1) {
                date = dateformat(dateToFormat, 'yyyy-mm-dd HH:MM:ss');

            } else {
                date = dateformat(dateToFormat, 'yyyy-mm-dd');

            }
            resolve(dateReport = date.replace(/[-:?^${}, +()|[\]\\ZT.]/g, ''))
        });
    }

}



module.exports = { getFileFTP, getReportMetricas }