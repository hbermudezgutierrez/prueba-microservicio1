const Order = require('../models/order.model'),
    gnConfig = require('../../config/general.config'),
    external = require('./external'),
    info = require("../../../package.json"),
    uuid = require("uuid").v4,
    lineReader = require('line-reader');

function process(file) {
    let registros = 0;
    let lineCount = 0
    let objs = [];
    let updatedObjs = [];

    try {
        lineReader.eachLine(file, async (line, last, cb) => {
            lineCount++
            // Preparación de la orden a partir de la línea del archivo
            let columns = line.split(gnConfig.reporte_ventas_separador);
            let usuarioSAP = columns[46];
            if (usuarioSAP == gnConfig.usuario_orquestador_sap) {
                let orderMapped = {
                    numero_pedido: columns[0],
                    estado_pedido: "FACTURADO",
                    numero_entrega: columns[1],
                    numero_factura: columns[2],
                    url_factura: await external.getURL({ invoice: columns[2] })
                }

                let payload = {
                    id_payload: uuid(),
                    numero_pedido: orderMapped.numero_pedido,
                    estado_pedido: "FACTURADO",
                    numero_entrega: orderMapped.numero_entrega,
                    numero_factura: orderMapped.numero_factura
                    //url_factura: orderMapped.url_factura
                }

                Order.findOne({ numero_pedido: orderMapped.numero_pedido }, (err, prevOrder) => {
                    if (!prevOrder || prevOrder.numero_factura !== orderMapped.numero_factura) {
                       
                        if (lineCount % 10000 === 0 || last) {
                            objs.push(orderMapped)
                            updatedObjs.push(orderMapped);
                            Order.insertMany(objs, async function (err, res) {
                                if (last) {
                                    if (err) {
                                        console.error('Error in insertMany', err)
                                    } else {
                                        console.log("en updatePedidos", objs);
                                        await external.updatePedidos({
                                            serviceid: info.name,
                                            version: info.version,
                                            route: "billing/job",
                                            task: "actualizar-masivo-pedidos-sap",
                                            description: "Tarea programada que obtiene el numero de factura un sftp",
                                            pedidos: objs
                                        });
                                        registros = registros + res.length;
                                    }
                                } else {
                                    objs = []
                                    return cb()
                                }
                            });
                        } else {
                            objs.push(orderMapped)
                            updatedObjs.push(orderMapped)
                            return cb()
                        }
                    } else {
                        if (last && objs.length > 0) {
                            Order.insertMany(objs, async function (err, res) {
                                if (err) {
                                    console.error('Error in insertMany', err)
                                } else {
                                    await external.updatePedidos({
                                        serviceid: info.name,
                                        version: info.version,
                                        route: "billing/job",
                                        task: "actualizar-masivo-pedidos-sap",
                                        description: "Tarea programada que obtiene el numero de factura un sftp",
                                        pedidos: objs
                                    });
                                    registros = registros + res.length;
                                }

                            });
                        } else {
                            return cb()
                        }
                    }
                });
            } else {
                if (!last) {
                    return cb()
                } else if (objs.length > 0) {
                    Order.insertMany(objs, async function (err, res) {
                        if (err) {
                            console.error('Error in insertMany', err)
                        } else {
                            console.log("en updatePedidos", objs);
                            await external.updatePedidos({
                                serviceid: info.name,
                                version: info.version,
                                route: "billing/job",
                                task: "actualizar-masivo-pedidos-sap",
                                description: "Tarea programada que obtiene el numero de factura un sftp",
                                pedidos: objs
                            });
                            registros = registros + res.length;
                        }
                    });
                }
            }
        })


    } catch (lineError) {
        console.error(lineError)
    }
}

module.exports = { process: process }