"use strict";

const Order = require('../models/order.model'),
    tmp = require('tmp'),
    fs = require('fs'),
    Files = require('../models/files.model'),
    ftConfig = require('../../config/sftp.config'),
    gnConfig = require('../../config/general.config'),
    external = require('./external'),
    Client = require('ssh2-sftp-client'),
    info = require("../../../package.json"),
    fileProcessor = require('./fileprocessor');

//Descarga del sftp archivos de reporte de ventas para su procesamiento.
function get () {
  let procesados = [];
  const conn = {
    host: ftConfig.sftp_host,
    port: ftConfig.sftp_port,
    username: ftConfig.sftp_user,
    password: ftConfig.sftp_pass
  };
  let sftp = new Client()
  sftp.connect(conn)
  .then(() => {    
    return sftp.list(ftConfig.file_remote_path);
  })
  .then((data) => {
      data.forEach((element) => {
        let fileName = element.name.split('.');
        if (element.name.indexOf(ftConfig.file_start_name) > -1 && fileName[1] == "TXT" || fileName[1] == "txt") {          
          Files.findOne({ name: element.name }, (err, myfile) => {
            if (!myfile) { 
              //Procesar archivo si no ha sido procesado
              sftp.get(`${ftConfig.file_remote_path}/${element.name}`)
                .then((buff) => {
                  let tmpObj = tmp.fileSync({ prefix: 'plan-', postfix: '.txt' })
                  const path = tmpObj.name
                  fs.appendFileSync(path, buff)                  
                  fileProcessor.process(tmpObj.name)                 

                  return element
                })
                .then(res => {
                  var file = new Files({ name: element.name, status: 'Procesado' });;
                  file.save(function (err) {
                    if (err) return console.error(err);
                    console.log(file.name + " saved to collection.");
                  });
                });
            } else {
              console.log('Archivo ya ha sido procesado:', element.name)
            }
          });
        }
      });      
  })
  .catch((err) => {
      console.log('catch error', err);
      sftp.end();
  });
  return procesados;
}

module.exports = { get }
