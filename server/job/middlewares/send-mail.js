"use strict";

const axios = require('axios'),
    gnConfig = require("../../config/general.config"),
    error = require("../../lib/error"),
    request = require("../../lib/request"),
    eventDispatcher = require('../../lib/dispatcher'),
    download = require("../../routes/download"),
    uuid = require("uuid").v4,
    { isNullOrEmpty, has, cloneObject } = require("../../lib/utilities"),
    notification = require('../cron-jobs/services/notification'),
    service = require('../../services/index'),
    constantes = require('../../constants/estado-pedido.constants'),
    fechas = require('../../lib/date');

async function exec(req) {
    let result = "";
    console.log("Inicia envio masivo Billing", req);
    try {
        let date = fechas.formatToYYYYMMDD(fechas.DateAdd('d', -gnConfig.dias_consulta_pedidos));
        if (!isNullOrEmpty(req) && has(req, 'fecha')) {
            date = req.fecha;
        }
        let consulta = `?estado_pedido=${gnConfig.estado_pedido_email}&limit=${gnConfig.limite_pedidos}`;
        consulta += `&fecha=${date}`;
        consulta += `&proceso_venta=${constantes.PROCESO_VENTA}`;
        consulta += `&tip_venta=${constantes.TIP_VENTA}`;

        console.log('consulta', consulta);
        const dt = await service.getOrdersDB(consulta);
        if (!isNullOrEmpty(dt) &&
            has(dt, "documents") &&
            Object.keys(dt.documents).length > 0) {
            // Se agrupan los pedidos por orden
            let mapOrdenes = dt.documents.reduce((acc, pedido) => {
                if (acc[pedido.id_process] !== undefined) {
                    let groupOrden = cloneObject(acc[pedido.id_process]);
                    let p = {};

                    groupOrden.tip_venta = pedido.tip_venta || groupOrden.tip_venta;
                    groupOrden.id_process = pedido.id_process || groupOrden.id_process;
                    groupOrden.proceso_venta = pedido.proceso_venta || groupOrden.proceso_venta;

                    p.numero_pedido = pedido.numero_pedido;
                    p.numero_factura = pedido.numero_factura;
                    p.url_factura = pedido.url_factura;
                    p.tip_venta = pedido.tip_venta;
                    p.tipo_pedido = pedido.tipo_pedido;

                    groupOrden.pedidos.push(p);
                    acc[pedido.id_process] = groupOrden;
                } else {
                    let ordenPedido = {};
                    let p = {};

                    ordenPedido.numero_orden = pedido.numero_orden;
                    ordenPedido.id_process = pedido.id_process || ordenPedido.id_process;
                    ordenPedido.email = pedido.cabecera.email;
                    ordenPedido.nombre_cliente = pedido.cabecera.nombre_cliente;
                    ordenPedido.tip_venta = pedido.tip_venta || ordenPedido.tip_venta;
                    ordenPedido.proceso_venta = pedido.proceso_venta || ordenPedido.tip_venta;
                    ordenPedido.cuentas = [];
                    ordenPedido.pedidos = [];

                    p.numero_pedido = pedido.numero_pedido;
                    p.numero_factura = pedido.numero_factura;
                    p.url_factura = pedido.url_factura;
                    p.tip_venta = pedido.tip_venta;
                    p.tipo_pedido = pedido.tipo_pedido;
                    ordenPedido.pedidos.push(p);
                    acc[pedido.id_process] = ordenPedido;
                }
                return acc;
            }, {});

            console.log('acc:', mapOrdenes);

            for (const orden in mapOrdenes) {
                let ordenpAux = [];
                mapOrdenes[orden].pedidos.forEach(ordenp => {
                    if (ordenpAux.length === 0) {
                        ordenpAux.push(ordenp);
                    }
                    if (ordenpAux.every(ordenAux => ordenAux.numero_pedido !== ordenp.numero_pedido)) {
                        ordenpAux.push(ordenp);
                    }
                })
                mapOrdenes[orden].pedidos = ordenpAux;
            }

            for (const orden in mapOrdenes) {
                if (mapOrdenes[orden].pedidos.length < 2 || !mapOrdenes[orden].pedidos.some(ordenAux => ordenAux.tipo_pedido === "Tangible")) {
                    delete mapOrdenes[orden];
                }
            }

            console.log('Ordenes con 2 productos:', mapOrdenes);

            // Se contruye JSON de entrada para enviar notificaciones por orden
            for (const numOrden in mapOrdenes) {
                let orden = mapOrdenes[numOrden];
                const template_email = {
                    id_template: "",
                    content_type: "",
                    params: {}
                }

                let urlFacturas = [];
                for (const pedido of orden.pedidos) {

                    //Actualizar URL de factura
                    let payload = {
                        id_process: orden.id_process,
                        numero_factura: pedido.numero_factura,
                        tipo_cliente: "S"
                    };

                    let urlFactura = await updateURL(payload);
                    if (urlFactura == false) throw new Error(`Error obteniendo la factura con id_process: ${orden.id_process}`);

                    pedido.url_factura = urlFactura;

                    // URL de adjuntos del correo
                    if (!isNullOrEmpty(pedido.url_factura)) {
                        urlFacturas.push({
                            numPedido: pedido.numero_pedido,
                            factura: pedido.url_factura
                        });
                    }

                    // Se identifican las lineas que fueron activadas por orden-pedido para
                    // incluirlas en el correo 
                    const activaDB = await getActivacionDB(`?numero_orden=${orden.numero_orden}&numero_pedido=${pedido.numero_pedido}`);
                    if (!isNullOrEmpty(activaDB) &&
                        has(activaDB, "documents") &&
                        Object.keys(activaDB.documents).length > 0) {

                        let cuentasActivas = [];
                        activaDB.documents.forEach(activacion => {
                            cuentasActivas.push(activacion.min);
                        });
                        orden.cuentas.push(cuentasActivas);
                    }
                }

                // set parametros 
                let params = {
                    cuenta: (JSON.stringify(orden.cuentas)).replace(/[.*+?^${}()|[\]\\\"]/g, ""),
                    cliente: orden.nombre_cliente,
                }

                let email = {
                    id_process: orden.id_process,
                    to: orden.email,
                    attached: urlFacturas,
                    subject: "Factura Tienda Virtual Claro ✔",
                    displayname: "Notificación Tienda Virtual Claro",
                    template_email: {
                        id_template: gnConfig.mail_contentType == 'MESSAGE' ? orden.tip_venta : gnConfig.mail_plantillaSimPaqueteId,
                        content_type: gnConfig.mail_contentType,
                        params
                    }
                }

                console.log("payload evento" + JSON.stringify(email));

                await notification.sendEmail(email)
                    .then(async () => {
                        let pedidos = [];
                        orden.pedidos.forEach(pedido => {
                            let temp = {
                                estado_pedido: constantes.NOTIFICADO,
                                numero_pedido: pedido.numero_pedido,
                                url_factura: pedido.url_factura
                            }
                            pedidos.push(temp);
                        });
                        // Se actualizan pedidos a NOTIFICADO.
                        if (!isNullOrEmpty(pedidos)) service.updatePedidos({ pedidos });
                    })
                    .catch(err => {
                        let messageError = `No se hizo el envio de correo para orden ${orden.numero_orden} - ${err.message}`
                        throw new Error(messageError);
                    });
            }
        }
        console.log("Termina envio masivo Billing", req);
    } catch (e) {
        result = error.errorHandler(e, result);
        console.log(e);
        result.message = "Error enviado el email de notificación";
    } finally {
        return result;
    }
};

async function updateURL(req) {
    let response;
    await download.orchParadigma(req)
        .then((dta) => {
            response = dta.documents.url;
        })
        .catch((e) => {
            console.log('===>> Hubo un error obteniendo la URL de la factura', e);
            response = false;
        });

    return response;
};

async function getActivacionDB(params) {
    try {
        const { data } = await axios({
            headers: { "content-type": "application/json" },
            method: "get",
            url: `${gnConfig.ruta_comunes}/comunes/activacion${params}`,
            preambleCRLF: true,
            postambleCRLF: true
        });
        return data;
    } catch (e) {
        if (gnConfig.logs) console.log("Error en getActivacionDB:", e);
    }
}

async function sendMailSap(data) {

    let email = {
        id_process: data.id_process,
        to: data.email,
        attached: [{
            numPedido: data.numero_pedido,
            factura: data.url_factura
        }],
        subject: "Factura Tienda Virtual Claro ✔",
        displayname: "Notificación Tienda Virtual Claro",
        template_email: {

            id_template: gnConfig.mail_plantillaSimPaqueteId,
            content_type: 'CONTENTID',
            params: data.params
        }

    }


    console.log("payload evento" + JSON.stringify(email))
    // Se emite evento para envío de notificación
    eventDispatcher.publish('StartEmailNotification', data.id_process, email).then(msgId => {
        console.log("Se envia evento StartEmailNotification");

    });
    let payload = {
        id_payload: uuid(),
        numero_pedido: data.numero_pedido,
        estado_pedido: "FACTURADO",
        numero_entrega: "",
        numero_factura: data.numero_factura,
        url_factura: data.url_factura,
        id_process: data.id_process,
        num_carrito: data.num_carrito,
        canal: data.canal,
        num_orden: data.num_orden
    }
    sendOrderBilled(payload)

}

async function sendOrderBilled(payload) {

    eventDispatcher.publish('OrderBilled', payload.id_payload, payload).then(msgId => {
        console.log(`Emite el evento para activar el financiamiento del pedido ${payload.numero_pedido}.`, msgId, payload);
    }).catch(e => {
        console.log(' Se generó un error en la emisión del evento OrderBilled >>> ', e);
    });
}
module.exports = { exec, sendMailSap };