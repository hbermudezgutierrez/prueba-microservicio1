'use strict';

const msg = require('../../class/message'),
    error = require('../../lib/error'),
    { DateAdd, formatToYYYYMMDD, formatToYYYYMMDDHHMMSS } = require('../../lib/date'),
    { dias_maximo_validacion_archivos } = require('../../config/general.config'),
    { requestEmailNotification, sendEmail } = require('./services/notification'),
    service = require('../../services/index'),
    order_status = require('../../constants/order-billing-state.constants'),
    { validateURLGenerationAndUpdateDB } = require('./process-file-billing'),
    { orderStatus } = require('../../routes/rfc-sap');


/**
 * Funcion para procesamiento de ordenes pendiente por notificacion de factura
 */
const jobSendEmailWithInvoice = async () => {
    let result = new msg.MessageBuilder()
        .setOrigen('billing/jobSendEmailWithInvoice')
        .setProcess(0)
        .build();
    try {
        console.log(`-- Job notificacion de facturacion [jobSendEmailWithInvoice] - ${formatToYYYYMMDDHHMMSS(new Date())}`);
        let generalData = {
            ordenes_pendientes_envio_correo: 0,
            info_notificacion: {}
        };

        await updateBillingURLPastDay();
        // consulta de ordernes facturadas
        let maximumDate = DateAdd('d', Number(-dias_maximo_validacion_archivos));
        let ordersPendingEmail = await service.findOrdersBilling({
            estado: order_status.FACTURA_GENERADA,
            updateDate: formatToYYYYMMDD(maximumDate)
        });

        if (ordersPendingEmail.length > 0) {
            generalData.ordenes_pendientes_envio_correo = ordersPendingEmail.length;
            generalData.info_notificacion = await validateSendEmailAndUpdateDB(ordersPendingEmail);
            result.success = true;
            result.message = "Execution successful";
            result.documents = generalData;
        } else {
            console.log('> No se encontraron ordenes pendiente por enviar correo.');
            result.message = 'No se encontraron ordenes pendiente por enviar correo estado [FACTURA_GENERADA]';
            result.documents = {
                fecha_maxima_update: formatToYYYYMMDD(maximumDate)
            }
        }
    } catch (err) {
        console.log(' Error en jobSendEmailWithInvoice > ', err.message);
        result = error.errorHandler(err, result);
    } finally {
        // guardado traza
        await service.saveServiceError(result);
    }
};

/**
 * Envia el email para ordenes y actuliza su estado en DB
 * @param {*} ordersDB [] ordenes para procesar
 * @returns {} informacion de procesamiento - numero de ordenes notificadas
 */
async function validateSendEmailAndUpdateDB(ordersDB) {
    return new Promise(async (resolve, reject) => {
        try {
            let info = {
                ordenes_notificadas: 0,
                ordenes_actualizadas: 0,
                info_errores: []
            };

            for (let order of ordersDB) {
                try {
                    await sendEmail(requestEmailNotification(order))
                        .then(async () => {
                            info.ordenes_notificadas++;
                            await service.updateOrderBilling({
                                estado: order_status.NOTIFICADO,
                                query: {
                                    numero_orden: order.numero_orden
                                }
                            }).then(r => info.ordenes_actualizadas++);

                            // actualizar estado de la orden
                            orderStatus(order.numero_orden, order_status.NOTIFICADO, order.tipo_venta);
                        })
                        .catch(err => {
                            info.info_errores
                                .push(`No se hizo el envio de correo para orden ${order.numero_orden} - ${err.message}`);
                        });
                } catch (errRequest) {
                    info.info_errores
                        .push(`No se hizo el envio de correo para orden ${order.numero_orden} - ${errRequest.message}`);
                    service.updateOrderBilling({
                        estado: order_status.ERROR_NOTIFICANDO,
                        query: {
                            numero_orden: order.numero_orden
                        }
                    });
                }
            }
            return resolve(info);
        } catch (e) {
            console.log(' Error validateSendEmailAndUpdateDB > ', e.message || e);
            return reject(e);
        }
    });
}

/**
 * Funcion que permite actualizar la URL de la factura si han pasado 23:30 horas
 * desde que se actulizo y sigue en FACTURADA
 * @returns {Number} Cantidad de ordenes que se actulizaron
 */
async function updateBillingURLPastDay() {
    return new Promise(async (resolve, reject) => {
        try {
            let pastDay = DateAdd('H', -23);
            pastDay = DateAdd('M', -45, pastDay);
            let ordersUpdateBilling = await service.findOrdersBilling({
                estado: order_status.FACTURA_GENERADA,
                updateDate: formatToYYYYMMDDHHMMSS(pastDay)
            });

            console.log(`Se actualizara la URL de la factura de [${ordersUpdateBilling.length}] orden(es). [${pastDay}]`);

            if (ordersUpdateBilling.length > 0) {
                await validateURLGenerationAndUpdateDB(ordersUpdateBilling)
                    .then(resUpdate => {
                        console.log('> Ordenes actualizadas validateURLGenerationAndUpdateDB > ', resUpdate);
                        return resolve(resUpdate);
                    }).catch(errUpdate => {
                        console.log(' Error actualizando la Factura > ', errUpdate.message);
                        return reject(errUpdate);
                    });
            }

            return resolve(0);
        } catch (e) {
            console.log('> Error en updateBillingURLPastDay > ', e.message || e);
            return reject(e);
        }
    });
}

module.exports = { jobSendEmailWithInvoice };