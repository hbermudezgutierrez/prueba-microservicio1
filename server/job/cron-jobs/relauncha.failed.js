
const gnConfig =require("../../config/general.config"),
request= require("../../lib/request"),
facturation=require("../../models/facturacion-venta.model")


/*
*Esta funcion se utiliza para relanzar las ordenes 
*que no quedaron en estado notificado usndo el servicio
*sendByBill
*/
const relaunchFailed = async ()=> {
    try {
            console.log("inicia relaunchFailed");
            let filter = { Estado_Proceso: { $ne: 'NOTIFICADO' } };
            facturation.find(filter, async (err, invoices) => {
                if (err) {
                    console.log(err);
                } else {
                   
                    for (let i = 0; i < invoices.length; i++) {
                        console.log("Ordenes a relanzar " +  invoices[i].Num_Orden);
                        let data =getReqRelaunchFailed(invoices[i]); 
                       
                        let dataSendByBill;
                        dataSendByBill = await request.sendByBill(data);
                        console.log("respuesta  sendByBill  para numero de orden  " + invoices[i].Num_Orden +"  "  +JSON.stringify(dataSendByBill));
             
                 
                    }
                }

            })

      
    } catch (error) {
        console.log(error);
    }
}

function getReqRelaunchFailed(data) {
    let req = {
        info_factura: {
            Num_Orden: data.Num_Orden,
            Num_pedido_SAP: data.Num_pedido_SAP,
            Nom_cliente: data.Nom_cliente,
            Email: data.Email,
            Tipo_proceso: data.Tipo_proceso,
            Canal: data.Canal

        }
    }
    return req;
}

module.exports = {
    relaunchFailed
};