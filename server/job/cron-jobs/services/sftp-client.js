'use strict';

const Client = require('ssh2-sftp-client');
const sftpConfig = require('../../../config/sftp.config');
const { DateAdd, formatToYYYYMMDD } = require('../../../lib/date');
const { reporte_ventas_separador, dias_maximo_validacion_archivos } = require('../../../config/general.config');
const service = require('../../../services/index');
const sftp = new Client();
const fs = require('fs');
const tmp = require('tmp');
const readline = require('readline');
const file_status = require('../../../constants/file-sftp-state.constants');

// configuracion de la conexion a al Sftp
const config = {
    host: sftpConfig.sftp_host,
    port: sftpConfig.sftp_port,
    username: sftpConfig.sftp_user,
    password: sftpConfig.sftp_pass
};


/**
 * Realiza la conexion al SFTP para extraer la lista de archivos filtrados,
 * se filtra por tipo de archivo, fecha e iniciales del nombre
 * @returns [] nombres de los archivos para procesar
 */
const getListFilesForProcess = async () => {
    let filesNames = [];
    return new Promise(async (resolve, reject) => {
        sftp.connect(config)
            .then(() => {
                console.log(`> Connect SFTP - Ruta [${sftpConfig.file_remote_path}]`);
                return sftp.list(sftpConfig.file_remote_path);
            })
            .then(files => {
                console.log(`Remote working directory have ${files.length} files`);
                let maxDate = DateAdd('d', Number(-dias_maximo_validacion_archivos));
                let filesFilter = files.filter(file => {
                    let dateModFile = new Date(file.modifyTime);
                    let extensionFile = file.name.split('.')[1];
                    return file.type != 'd' && file.name.includes(sftpConfig.file_start_name) && dateModFile > maxDate
                        && extensionFile.toUpperCase() == 'TXT';
                });

                filesNames = filesFilter.map(fil => fil.name);
                console.log(`fecha maxima de procesamiento de archivos - ${formatToYYYYMMDD(maxDate)} 
                total de archivos ${filesNames.length}`);
                resolve(filesNames);
            })
            .catch(err => {
                console.log(`Error getListFilesForProcess - ${err.message}`);
                reject(err);
            });
    });
}


/**
 * Se procesa el archivo del sftp para extraer los datos
 * @param {*} fileName nombre del archivo para procesar 
 * @returns Map - con los datos relevantes de la lectura del archivo {numero pedido - numero de factura}
 */
const getDataFile = async (fileName) => {
    return new Promise(async (resolve, reject) => {

        await sftp.get(`${sftpConfig.file_remote_path}/${fileName}`)
            .then((buff) => {
                let tmpObj = tmp.fileSync({ prefix: 'plan-', postfix: '.txt' });
                const path = tmpObj.name;
                fs.appendFileSync(path, buff);
                return resolve(readFileExtractData(path));
            })
            .catch(err => {
                // error en la lectura del archivo - Actualizar estado
                service.saveInfoFile({ nombre_archivo: fileName, estado: file_status.ERROR_PROCESANDO });
                console.log('Error en la lectura de archivos SFTP > ', err.message);
                return reject(err);
            });
    });
};


/**
 * Funcion que lee el archivo para retornar un Map con los datos
 * @param {*} pathFile File para procesar 
 * @returns Map  key: numero de pedido - valores: numero de factura
 */
async function readFileExtractData(pathFile) {
    const readInterface = readline.createInterface({
        input: fs.createReadStream(pathFile),
        output: process.stdout,
        console: false
    });
    return new Promise((res, rej) => {
        try {
            let valuesOfFile = new Map();
            readInterface.on('line', function (line) {
                let valuesLines = line.split(reporte_ventas_separador);
                // key numero de pedido - Values: numero de factura y numero de entrega
                valuesOfFile.set(valuesLines[0], { numero_factura: valuesLines[2], numero_entrega: valuesLines[1] });
            }).on('close', function () {
                return res(valuesOfFile);
            });
        } catch (err) {
            console.log('> Error readFileExtractData > ' + err);
            return rej(err);
        }
    });

}

function closeConection() {
    console.log(' -- Session SFTP closed -- ');
    sftp.end();
}

module.exports = {
    getListFilesForProcess,
    getDataFile,
    closeConection,
};