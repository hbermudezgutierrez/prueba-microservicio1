'use strict';

const axios = require("axios"),
    gnConfig = require('../../../config/general.config'),
    { has, isNullOrEmpty } = require('../../../lib/utilities'),
    valuesOrder = require('../../../constants/order-tipo-proceso-venta.constants');

/**
 * Funcion para consumo de ms-notification /notification/email
 * @param {*} request consumo de ms-notificacion
 * @returns response de consumo de ms-notification - procesado
 */
const sendEmail = async (request) => {
    console.log('> Send email REQUEST > ', request);
    return new Promise(async (resolve, reject) => {
        await axios.post(`${gnConfig.ruta_notifications}/notification/email`, request)
            .then(res => {
                console.log('> Response ms-notification > ', res.data || res);
                if (has(res, 'data') && has(res.data, 'success') && res.data.success) {
                    return resolve(res.data);
                } else if (has(res, 'data') && has(res.data, 'message')) {
                    return reject({ message: res.data.message });
                } else {
                    return reject(res);
                }
            })
            .catch(e => {
                console.log(' - Error en sendEmail > ', e.message || e);
                return reject(e);
            });
    });
}

/**
 * Se construye request para consumo de notification
 * @param {*} order estructura de order-billing.model
 * @returns Request consumo de ms-notification
 */
const requestEmailNotification = (order) => {
    if (has(order.cliente, 'correo') && !isNullOrEmpty(order.cliente.correo)) {
        let request = {
            id_process: order.id_process,
            to: order.cliente.correo,
            subject: gnConfig.mail_subject,
            displayname: gnConfig.mail_displayname,
            attached: [{
                numPedido: order.numero_pedido,
                factura: order.factura.url
            }]
        };

        if (order.proceso_venta == valuesOrder.PROCESO_VENTA_KIT
            && order.tipo_venta == valuesOrder.TIPO_VENTA_PREPAGO) {

            request['template_email'] = {
                id_template: gnConfig.email_template_orden_entregado_cliente,
                content_type: gnConfig.email_content_type
            };
            return request;
        }
        else if (order.proceso_venta == valuesOrder.PROCESO_VENTA_VTA
            && order.tipo_venta == valuesOrder.TIPO_VENTA_PREVENTA) {

            request['template_email'] = {
                content_type: "MESSAGE",
                message_content: "<html><body><p>Buen D&iacute;a usuario,<br></p><p><strong>Prueba PREVENTA</strong> !!. </body></html>"
            };
            return request;

        } else if (order.tipo_venta == valuesOrder.TIPO_VENTA_LIBRE) {

            request['template_email'] = {
                content_type: "MESSAGE",
                message_content: "<html><body><p>Buen D&iacute;a usuario,<br></p><p><strong>Esto es una prueba TIPO VENTA LIBRE </strong> !!. </body></html>"
            };

            return request;

        } else {
            throw new Error(' No hay un request de correo configurado para la orden');
        }
    } else {
        throw new Error('La orden tiene el email vacio - No se puede enviar notificacion');
    }

};

module.exports = { sendEmail, requestEmailNotification };