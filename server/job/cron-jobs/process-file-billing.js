'use strict';

const { getDataFile, getListFilesForProcess, closeConection } = require('./services/sftp-client'),
    msg = require('../../class/message'),
    error = require('../../lib/error'),
    { DateAdd, formatToYYYYMMDD, formatToYYYYMMDDHHMMSS } = require('../../lib/date'),
    { dias_maximo_validacion_archivos, tipo_cliente_consumo_paradigma } = require('../../config/general.config'),
    service = require('../../services/index'),
    { has, isNullOrEmpty } = require('../../lib/utilities'),
    { orchParadigma } = require('../../routes/download'),
    order_status = require('../../constants/order-billing-state.constants'),
    file_status = require('../../constants/file-sftp-state.constants'),
    { TIPO_VENTA_LIBRE, TIPO_PAGO_FINANCIADO } = require('../../constants/order-tipo-proceso-venta.constants'),
    { publishEventOrderBilled } = require('../../dispatcher/activate-confirmation.event');

/**
 * Funcion para ejeucccion de Job de procesamiento de archivos SAP y generacion de factura paradigma
 */
const jobProcessFilesAndBilling = async () => {
    let result = new msg.MessageBuilder()
        .setOrigen('billing/jobProcessFilesAndBilling')
        .setProcess(0)
        .build();
    try {
        console.log(` -- Se ejecuta JOB procesamiento de archivos SAP y generacion de factura [jobProcessFilesAndBilling] - ${formatToYYYYMMDDHHMMSS(new Date())}`);

        let filesToProcess = await getListFilesForProcess();
        filesToProcess = await validateFileHasProcess(filesToProcess);

        if (filesToProcess.length > 0) {
            result.message = "Execution successful";
            result.success = true;
            result.documents = { archivos_procesados: filesToProcess };
            const responseToProcess = await processFilesSftpSAP(filesToProcess);
            result.documents = { ...result.documents, ...responseToProcess };
            const responseGenerateInvoice = await getInvoiceURL();
            result.documents = { ...result.documents, ...responseGenerateInvoice };
        } else {
            closeConection();
            result.success = true;
            result.message = 'No hay archivos pendientes por procesar en SFTP';
            console.log(' --- NO hay archivos pendientes para procesar por job facturacion --- ');
        }
    } catch (err) {
        console.log(' Error en jobProcessFilesAndBilling > ', err);
        result = error.errorHandler(err, result);
    } finally {
        // guardado traza
        await service.saveServiceError(result);
    }
};




/**
 * Se valida que archivos no se ha procesado por el Job
 * @param {*} allFiles [] nombres de archivos de SFTP
 * @returns [] Files que no se han procesado por el Job
 */
async function validateFileHasProcess(allFiles) {
    let maximumDate = DateAdd('d', Number(-dias_maximo_validacion_archivos));
    let fileDBProcess = await service.findFile({ estado: file_status.PROCESADO, startDate: formatToYYYYMMDD(maximumDate) });
    fileDBProcess = fileDBProcess.map(fil => fil.nombre_archivo);
    return allFiles.filter((file) => !fileDBProcess.includes(file));
};


/**
 * Funcion para procesar los archivos SAP del SFTP hacer merge con los pedidos/ordenes de DB
 * @param {*} filesNames [] nombres de los archivos para procesar
 * @returns {} con informacion relacionada al proceso, con numero de casos exitosos
 */
async function processFilesSftpSAP(filesNames) {
    let allDataFiles = new Map();
    for (let file of filesNames) {
        await getDataFile(file).then(async (dataFile) => {
            if (allDataFiles.size == 0)
                allDataFiles = new Map([...dataFile]);
            else
                allDataFiles = new Map([...allDataFiles, ...dataFile]);

            console.log(` Lectura del archivo ${file} - Numero de registros ${dataFile.size} `);
            // actualizar como archivo -- procesado
            let fileProcess = {
                nombre_archivo: file,
                estado: file_status.PROCESADO,
                registros_procesados: dataFile.size
            };
            await service.saveInfoFile(fileProcess);
            await delay(1000);
        });
    }
    closeConection();
    // match entre pedidos contabilizados - datos archivos txt
    return await mergeDataDBWithFilesSAP(allDataFiles);
}

/**
 * Funcion para cruzar informacion de los archivos con la bases de datos de ordenes [CONTABILIZADO]
 * @param {*} dataFileSAP MAP - informacion de pedido y numero de factura
 * @returns {} informacion simplificada del proceso - numero de ordenes procesadas
 */
async function mergeDataDBWithFilesSAP(dataFileSAP) {

    let dataMerge = {
        ordenes_contabilizadas: 0,
        ordenes_archivos_sap: dataFileSAP.size,
        ordenes_actualizadas: 0
    };

    // consulta de ordernes contabilizadas
    let maximumDate = DateAdd('d', Number(-dias_maximo_validacion_archivos));
    let ordersContabilizadas = await service.findOrdersBilling({
        estado: order_status.CONTABILIZADO,
        startDate: formatToYYYYMMDD(maximumDate)
    });

    if (ordersContabilizadas.length > 0) {
        dataMerge.ordenes_contabilizadas = ordersContabilizadas.length;
        dataMerge.ordenes_actualizadas = await validateDataAndUpdateDB(ordersContabilizadas, dataFileSAP);
    }

    return dataMerge;
}

/**
 * Cruce de ordenes de base de datos con informacion de los archivos
 * @param {*} ordersDB [] ordenes contabilizadas
 * @param {*} dataFiles Map - Datos de archivos SAP
 * @returns numero de ordenes actulizadas en basr de datos
 */
async function validateDataAndUpdateDB(ordersDB, dataFiles) {
    return new Promise(async (resolve, reject) => {
        try {
            let numOrdersUpdate = 0;
            for (let order of ordersDB) {
                if (dataFiles.has(order.numero_pedido)) {
                    let dtaPedido = dataFiles.get(order.numero_pedido);
                    // actulizar informacion con numero de factura - estado
                    let updateOrder = {
                        estado: order_status.FACTURADO,
                        numero_factura: dtaPedido.numero_factura,
                        query: {
                            numero_pedido: order.numero_pedido
                        }
                    };

                    await service.updateOrderBilling(updateOrder).then(r => numOrdersUpdate++);

                    // validacion para emision de Evento - OrderBilled
                    console.log(`Validacion Evento OrderBilled para [${order.numero_orden}] > ${order.tipo_venta} -- ${order.tipo_pago}. `);

                    if (order.tipo_venta == TIPO_VENTA_LIBRE && !isNullOrEmpty(order.tipo_pago)
                        && order.tipo_pago == TIPO_PAGO_FINANCIADO) {
                        await publishEventOrderBilled({
                            id_process: order.id_process,
                            id_payload: order.id_process,
                            numero_pedido: order.numero_pedido,
                            estado_pedido: order_status.FACTURADO,
                            numero_entrega: dtaPedido.numero_entrega,
                            numero_factura: dtaPedido.numero_factura
                        });
                    }
                }
            }
            resolve(numOrdersUpdate);
        } catch (e) {
            console.log(' Error en validateDataAndUpdateDB > ', e.message || e);
            reject(e);
        }
    });
}

/**
 * Consulta de la ordenes listas para generar su URL de factura en paradigma
 * @returns {} datos especificos de procesamiento - numero de ordenes facturadas
 */
async function getInvoiceURL() {
    return new Promise(async (resolve, reject) => {
        try {

            let generalData = {
                ordenes_facturadas: 0,
                ordenes_factura_generada: 0
            };

            // consulta de ordernes facturadas
            let maximumDate = DateAdd('d', Number(-dias_maximo_validacion_archivos));
            let ordersInvoices = await service.findOrdersBilling({
                estado: order_status.FACTURADO,
                startDate: formatToYYYYMMDD(maximumDate)
            });

            if (ordersInvoices.length > 0) {
                generalData.ordenes_facturadas = ordersInvoices.length;
                generalData.ordenes_factura_generada = await validateURLGenerationAndUpdateDB(ordersInvoices);
            }
            resolve(generalData);
        } catch (e) {
            console.log(' Error en getInvoiceURL > ', e.message || e);
            reject(e);
        }
    });
}

/**
 * Consumo de servicio de paradigma y actulizacion en base de datos de informacion URL
 * @param {*} ordersDB [] ordenes para generar URL de factura
 * @returns numero de ordenes actulizadas en base de datos
 */
async function validateURLGenerationAndUpdateDB(ordersDB) {
    return new Promise(async (resolve, reject) => {
        try {
            let numOrdersUpdate = 0;
            let dataService = {
                task: "billing-obtener-factura",
                description: "Se obtiene la factura pdf desde paradigma",
                service: 'pdf',
                method: 'factura',
                tipo_cliente: tipo_cliente_consumo_paradigma
            };

            for (let order of ordersDB) {
                dataService['id_process'] = order.id_process;
                dataService['numero_factura'] = order.factura.numero;
                let responseParadigma = await orchParadigma(dataService);

                if (has(responseParadigma, 'success') && responseParadigma.success
                    && has(responseParadigma, 'documents') && has(responseParadigma.documents, 'url')) {
                    let updateOrder = {
                        estado: order_status.FACTURA_GENERADA,
                        url_factura: responseParadigma.documents.url,
                        query: {
                            numero_orden: order.numero_orden
                        }
                    };
                    await service.updateOrderBilling(updateOrder).then(r => numOrdersUpdate++);
                }
            }
            resolve(numOrdersUpdate);
        } catch (e) {
            console.log(' Error en validateURLGenerationAndUpdateDB > ', e.message || e);
            reject(e);
        }
    });
}

/**
 * Funcion de espera 
 * @param {*} ms numero de milisegundos de espera
 * @returns 
 */
function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


module.exports = { jobProcessFilesAndBilling, validateURLGenerationAndUpdateDB };