'use strict'

const { jobProcessFilesAndBilling } = require('../cron-jobs/process-file-billing'),
    { jobSendEmailWithInvoice } = require('../cron-jobs/send-email'),
    { minutesTomillis, formatToYYYYMMDDHHMMSS, DateAdd, formatHHMMSS } = require('../../lib/date'),
    { tiempo_espera_envio_email, tiempo_espera_reenvio_email } = require('../../config/general.config');


/**
 * Funcion que orquesta le ejecuccion del Job de facturacion
 */
const schedulerFacturar = () => {

    console.log(' > Job procesamiento de archivos SFTP - SAP y generacion de factura < ');
    setImmediate(jobProcessFilesAndBilling);

    setTimeout(jobSendEmailWithInvoice, minutesTomillis(tiempo_espera_envio_email));

    setTimeout(jobSendEmailWithInvoice, minutesTomillis(tiempo_espera_reenvio_email));
}

/**
 * Funcion para obtener informacion de procesamiento de Job
 * @returns {} datos de procesamiento del job
 */
function jobProcessingInformation() {
    return {
        descripcion: `Se ejecuta proceso [schedulerFacturar] Job de facturacion  - ${formatToYYYYMMDDHHMMSS(new Date())}`,
        incia_envio_email_: {
            hora_aprox_envio: formatHHMMSS(DateAdd('M', Number(tiempo_espera_envio_email))),
            hora_aprox_reenvio: formatHHMMSS(DateAdd('M', Number(tiempo_espera_reenvio_email)))
        }
    };
}


module.exports = {
    schedulerFacturar,
    jobProcessingInformation
}