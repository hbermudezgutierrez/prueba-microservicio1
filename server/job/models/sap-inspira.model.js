"use strict";

const mongoose = require('mongoose'),
  dbConfig = require('../../config/database.config');

const SapInspiraSchema = mongoose.Schema({
  num_orden: String, 
  num_pedido: String, 
  num_factura: String, 
  serial: String, 
  estado: String, 
  doc_entrega : String, 
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('SapInspira', SapInspiraSchema, dbConfig.sap_inspira_collection)