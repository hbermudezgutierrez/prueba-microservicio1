"use strict";

const mongoose = require('mongoose'),
  dbConfig = require("../../config/database.config");

const OrderSchema = mongoose.Schema({
  numero_pedido: String,
  numero_entrega: String,
  numero_factura: String, 
  url_factura: String
}, {
  timestamps: true,
  versionKey: false
});

module.exports = mongoose.model('Orders', OrderSchema, dbConfig.orders_collection)