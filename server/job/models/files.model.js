"use strict";

const mongoose = require('mongoose'),
  dbConfig = require('../../config/database.config');

const FileSchema = mongoose.Schema({
  name: String,
  status: String
}, {
  timestamps: true,
  versionKey: false
})

module.exports = mongoose.model('Files', FileSchema, dbConfig.files_collection)