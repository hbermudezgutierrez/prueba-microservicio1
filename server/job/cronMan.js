'use strict'

const cron = require('node-cron'),
  { cron_expression_job_facturacion } = require('../config/general.config'),
  { schedulerFacturar } = require("./scheduler/scheduler-facturar"),
  { formatToDDMMYYYYHHMMSS } = require('../lib/date');

/**
 * Funcion que programa el proceso de facturacion para ejecuccion segun parametrizacion
 */
function runJobFacturacion() {
  try {
    cron.schedule(cron_expression_job_facturacion, () => {
      console.log(' -- Running a task <schedulerFacturar> ' + formatToDDMMYYYYHHMMSS(new Date()));
      schedulerFacturar();
    });
  } catch (err) {
    console.log('> Error runJobFacturacion ', err);
  }
}

module.exports = { runJobFacturacion };