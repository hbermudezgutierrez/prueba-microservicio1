"use strict";

module.exports = (app) => {
  const invoices = require('../controllers/invoice.controller')

  // handle /ls path
  app.get('/ls', invoices.ls)
}