"use strict";


const _ = require('lodash'),
    msg = require('../class/message'),
    info = require('../../package.json'),
    trace = require('../lib/trace'),
    validate = require('../lib/validate'),
    { cloneObject, has, isNullOrEmpty, isArray } = require('../lib/utilities'),
    { errorHandler } = require('../lib/error'),
    { contabilizarSAP, getOrdersCollOrder } = require('../services/index'),
    constOrder = require('../constants/order-tipo-proceso-venta.constants');

/**
 * Funcion que permite procesar el topico OrderDeliveredCustomerTopic 
 * para activacion de KIT prepago
 * @param {*} payload 
 */
async function handlerOfTopicOrderDeliveredCustomerTopic(payload) {
    const result = new msg.MessageBuilder()
        .setOrigen('factura/OrderDeliveredCustomerTopic')
        .setProcess(payload.channelOrderNbr)
        .build();
    let dataCache = {};

    console.log(' >>> handlerOfTopicOrderDeliveredCustomerTopic > ', payload);
    try {

        payload = getInfoToCache(payload);
        dataCache.request = cloneObject(payload);
        dataCache.request.status = "FALLIDO";

        // validate payload
        validate.eventOrderDeliveredCustomer(payload);

        // get de la orden - para validar si es KIT
        let dataOrders = await getOrdersCollOrder({ num_orden: payload.channelOrderNbr });
        let numero_orden = payload.channelOrderNbr;
        result.process = payload.channelOrderNbr;

        // existe la orden
        if (excecutionSuccessful(dataOrders) && dataOrders.documents.length > 0) {
            let order = dataOrders.documents[0];
            let pedido = order.pedidos[0];
            let id_process = order.id_process;
            result.process = id_process;
            dataCache.request.id_process = id_process;

            let isKitPrepago = pedido.proceso_venta === constOrder.PROCESO_VENTA_KIT
                && pedido.tip_entrega === constOrder.TIPO_ENTREGA_CAV
                && pedido.tip_venta === constOrder.TIPO_VENTA_PREPAGO;
            console.log('> Orden isKitPrepago >> ', isKitPrepago);
            if (isKitPrepago) {

                let productosContabilizar = payload.products;
                console.log(' productosContabilizar>> ', productosContabilizar);
                let infoSAP = _.map(productosContabilizar, 'sapInfo')[0];
                console.log(' >>>> infoSAP >> ', infoSAP);

                await contabilizarSAP(numero_orden, productosContabilizar, infoSAP.sapDeliveryId)
                    .then(responseConta => {
                        dataCache.request.status = "COMPLETO";
                        result.success = true;
                        result.status = 200;
                        result.message = 'Execution successful';
                        result.documents = responseConta;
                    }).catch(errService => {
                        if (isArray(errService))
                            result.documents = errService;
                        else
                            errorHandler(errService, result)
                    });
            } else {
                result.message = `No se procesa la orden [${numero_orden}] porque no corresponde a kit prepago.`;
            }
        } else {
            result.message = `No se encontro la orden [${numero_orden}] en coll_orders.`;
        }

    } catch (err) {
        console.log(' Error [handlerOfTopicOrderDeliveredCustomerTopic] ', err);
        errorHandler(err, result);
    } finally {
        // actualizacion en cache
        dataCache.response = result;
        trace.runAll(dataCache);
    }


}

function getInfoToCache(dta) {

    dta.serviceid = info.name;
    dta.version = info.version;
    dta.service = 'factura';
    dta.method = 'OrderDeliveredCustomerTopic';
    dta.task = "procesar-evento-OrderDeliveredCustomerTopic";
    dta.description = "Se procesa evento de entrega al usuario para contabilizacion de Kit Prepago.";
    return dta;
}


/**
 * Funcion que valida una ejecuccion exitosa de un servicio
 * @param {*} response del servicio
 * @returns true si fue exitosa
 */
function excecutionSuccessful(response) {
    return has(response, 'success') && response.success && has(response, 'documents');
}


module.exports = {
    handlerOfTopicOrderDeliveredCustomerTopic
}
