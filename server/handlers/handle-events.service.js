"use strict";

const gnConfig = require("../config/general.config"),
  msg = require("../class/message"),
  trace = require("../lib/trace"),
  error = require("../lib/error"),
  axios = require("axios"),
  { cloneObject, isNullOrEmpty, has } = require('../lib/utilities'),
  { getReqGenerales } = require('../lib/request'),
  { getOrder, sapNumeroFactura } = require('../routes/rfc-sap'),
  { orchParadigma } = require('../routes/download'),
  { sendMailSap } = require('../job/middlewares/send-mail'),
  { validateEventDeliveryEntrega } = require('../lib/validate'),
  service = require('../services/index'),
  order_status = require('../constants/order-billing-state.constants');


/**
 * Manejo de evento OrderInstalled para generar factura SAP
 * generar URL de factura y enviar notificacion
 * @param {*} event 
 */
const handleOrderInstalled = async (event) => {
  console.log('----  Manejando evento handleOrderInstalled   ---');
  let jsonEvent = JSON.parse(event)
  let payload = cloneObject(jsonEvent.payload);

  console.log(" --- event payload >> ", payload);

  // se consulta la orden 
  let paramsOrden = {
    id_process: payload.id_process
  };
  let ordenDB = await getOrder(paramsOrden);

  if (has(ordenDB, 'documents') && !isNullOrEmpty(ordenDB.documents[0])) {
    let orden = ordenDB.documents[0];

    // ordenes de Inspira - Mi asesor Claro
    if (orden.canal == 'ECI_B2C' || orden.canal == 'MCA_B2C') {
      // Ejecuccion de  RFC

      let reqRfcSap = {
        flow: true,
        id_process: orden.id_process
      };

      // ajuste de data para caché
      reqRfcSap = getReqGenerales(reqRfcSap);
      reqRfcSap.service = "factura";
      reqRfcSap.method = "pdf";
      reqRfcSap.task = "billing-generar-factura";
      reqRfcSap.description = "Se obtiene el numero de factura generado desde SAP";


      let responseRFC = await sapNumeroFactura(reqRfcSap);
      console.log(' Response RFC  >>> ', responseRFC);

      // validacion ejecuccion correcta generacion de factura SAP
      if (has(responseRFC, 'success') && responseRFC.success
        && has(responseRFC, 'documents') && has(responseRFC.documents, 'numeroFacturaSAP')) {

        /* let num_factura = responseRFC.documents.numeroFacturaSAP;

        let reqFactura = {
          id_process: orden.id_process,
          numero_factura: num_factura,
          tipo_cliente: "S"
        }

        if (gnConfig.logs) console.log(' >>> Request orchParadigma  >>> ', reqFactura);
        // ajuste de data para caché 
        reqFactura = getReqGenerales(reqFactura);
        reqFactura.service = "factura";
        reqFactura.method = "numero-factura";
        reqFactura.task = "billing-obtener-factura";
        reqFactura.description = "Se obtiene la factura pdf desde paradigma";

        let responseFactura = await orchParadigma(reqFactura);
        console.log(' >>> response  PARAGIMA >>> ', responseFactura);

        // validacion correcta de generacion de factura en paradigma
        if (has(responseFactura, 'success') && responseFactura.success
          && has(responseFactura, 'documents') && has(responseFactura.documents, 'url')) {

          let url_factura = responseFactura.documents.url;
          // request Email
          let params = {
            cuenta: orden.cliente.telef_movil,
            cliente: orden.cliente.nombres
          }

          let reqEmail = {
            ...params,
            id_process: orden.id_process,
            email: orden.cliente.email,
            url_factura: url_factura,
            numero_pedido: orden.pedidos[0].numero_pedido,
            numero_factura: num_factura,
            num_carrito: orden.num_carrito,
            canal: orden.canal,
            num_orden: orden.num_orden
          }

          if (gnConfig.logs) console.log('>>> REQ EMAIL >> ', reqEmail);

          // Ejecucion EMAIL
          sendMailSap(reqEmail);

        }; */
      };
    };
  };
};


/**
 * Manejo de evento ---  para Kit prepago
 * para generar la contabilizacion
 * generacion de factura y envio de correo
 * @param {*} event 
 */
const handleDeliveryOrderEntregado = async (event) => {
  let result = new msg.MessageBuilder()
    .setOrigen(`--/msDeliveryOrderEntregadoCliente`).build();

  console.log('----  Manejando evento msDeliveryOrderEntregadoCliente   ---', event);
  let jsonEvent;

  try {
    jsonEvent = JSON.parse(event);
    let payload = cloneObject(jsonEvent);

    // validacion de estructura - validacion de estado
    validateEventDeliveryEntrega(payload);

    //Consulta de orden y validacion de tipo de orden
    let params = { id_omnicanal: payload.aggregateId };
    let dataOrders = await getOrder(params);

    if (excecutionSuccessful(dataOrders) && dataOrders.documents.length > 0) {

      const order = dataOrders.documents[0];
      const pedido = order.pedidos[0];

      let isKitPrepago = pedido.proceso_venta == 'KIT' && pedido.tip_entrega == 'PV'
        && pedido.tip_venta == 'Prepago';
      console.log(' isKitPrepago >> ', isKitPrepago);

      if (isKitPrepago) {

        params = {
          flow: true,
          id_process: order.id_process,
          documents: {
            numero_orden: order.num_orden
          }
        };

        // contabilizacion SAP 
        const { data } = await axios.post(`${gnConfig.ruta_orders}/pedido/contabiliza`, params);
        console.log(' Response RFC CONTABILIZACION >>> ', data);

        // validacion ejecuccion correcta contabilizacion 
        if (excecutionSuccessful(data)) {
          // guardado en base de datos - coll_orders_billing
          let ordenContabilizada = {
            id_process: order.id_process,
            numero_orden: order.num_orden,
            numero_pedido: pedido.numero_pedido,
            canal: pedido.canal,
            proceso_venta: pedido.proceso_venta,
            tipo_venta: pedido.tip_venta,
            cliente: {
              nombre: order.cliente.nombres,
              telefono: order.cliente.telef_movil,
              correo: order.cliente.email
            },
            logs_estados: [{ estado: order_status.CONTABILIZADO }]
          };
          await service.saveOrderBilling(ordenContabilizada);

        };
      };

    };

  }
  catch (e) {
    result = error.errorHandler(e, result);
    result.req_event = jsonEvent;
    trace.errors(result);
  }
};


/**
 * Funcion que valida una ejecuccion exitosa de un servicio
 * @param {*} response del servicio
 * @returns true si fue exitosa
 */
function excecutionSuccessful(response) {
  return has(response, 'success') && response.success && has(response, 'documents');
}

module.exports = { handleOrderInstalled, handleDeliveryOrderEntregado }