'use strict';

const ServiceErrorSchema = require('../models/service-errors.model');
const { setIfNotEmpty } = require('../lib/utilities');

/**
 * Funcion que permite guardar la traza de las funciones que se ejecutan
 * @param {*} dtaServiceErr Object Model service-errors.model
 * @returns Informacion detallada del guardado del documento
 */
const saveServiceError = async (dtaServiceErr) => {
    return new Promise((resolve, reject) => {
        let serviceError = new ServiceErrorSchema({ ...dtaServiceErr });
        serviceError.save(function (err, doc) {
            if (err)
                return reject(err);
            console.log("Document <ServiceErrorSchema> inserted successfully!");
            return resolve(doc);
        });
    });
};

/**
 * Funcion que permite buscar los documento de la coleccion service_errors
 * @param {*} params filtros para busqueda de documentos
 * @returns [] documents segun el filtro
 */
const findErrors = async (params) => {
    return new Promise((resolve, reject) => {
        let filter = {};
        setIfNotEmpty(filter, 'id_process', params.id_process);
        setIfNotEmpty(filter, 'origen', params.origen);
        setIfNotEmpty(filter, 'success', params.success);
        console.log(' filter FindErrors >> ', filter);
        ServiceErrorSchema.find(filter)
            .lean()
            .sort({ createdAt: -1 })
            .exec((err, resultDB) => {
                if (err)
                    return reject(err);
                console.log(' FindErrors >> ', resultDB.length);
                return resolve(resultDB);
            });
    });
};



module.exports = {
    saveServiceError,
    findErrors
};