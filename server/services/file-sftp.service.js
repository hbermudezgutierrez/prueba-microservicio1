'use strict';

const FileSchema = require('../models/files-sftp.model');
const { setIfNotEmpty, isNullOrEmpty } = require('../lib/utilities');


/**
 * Funcion que permite almacenar en DB la informacion de validacion para procesamiento
 *  los archivos de SFTP 
 * @param {*} dtaFile Object Modelo de files-sftp.model
 * 
 */
const saveInfoFile = async (dtaFile) => {
    return new Promise((resolve, reject) => {
        let fileSchema = new FileSchema({ ...dtaFile });
        fileSchema.save(function (err, doc) {
            if (err)
                return reject(err);
            console.log("Document <FileSchema> inserted successfully!");
            return resolve(doc);
        });
    });
};

/**
 * Funcion que permite buscar los documento de la coleccion coll_files_sftp
 * @param {*} params filtros para busqueda de documentos
 */
const findFile = async (params) => {
    return new Promise((resolve, reject) => {
        let filter = {};
        setIfNotEmpty(filter, 'nombre_archivo', params.nombre_archivo);
        setIfNotEmpty(filter, 'estado', params.estado);
        if (!isNullOrEmpty(params.startDate))
            filter['createdAt'] = { $gte: new Date(`${params.startDate} 00:00`) };

        console.log(' filter findFile >> ', filter);
        FileSchema.find(filter)
            .lean()
            .exec((err, resultDB) => {
                if (err)
                    return reject(err);
                console.log(' findFile >> ', resultDB.length);
                return resolve(resultDB);
            });
    });

};



module.exports = {
    saveInfoFile,
    findFile
};