

const axios = require('axios'),
    gnConfig = require('../config/general.config'),
    { has, isNullOrEmpty, isArray } = require('../lib/utilities'),
    constStatus = require('../constants/estado-update-orden.constants'),
    { orderStatusUpdateNotification } = require('./pedidos-sap.service');


async function contabilizarSAP(numero_orden, products, numero_entrega) {
    return new Promise(async (resolve, reject) => {
        try {
            let estado_contabilizacion = constStatus.CONTABILIZACION_ERR;
            // construir Request 
            let requestSAPContabilizar = getReqContabilizar(numero_entrega, products);
            console.log(' >>> requestSAPContabilizar ', JSON.stringify(requestSAPContabilizar));

            // consumir SAP
            await consumeRfcSap(requestSAPContabilizar)
                .then(responseSAP => {
                    // validar response OK contabilizacion 
                    if (!isNullOrEmpty(responseSAP) && isArray(responseSAP)) {
                        let existOneSuccessful = responseSAP.some((element) => {
                            if (has(element, 'NRO_ENTREGA') && has(element, 'NRO_DOC_MATERIAL'))
                                return true;
                            return false;
                        });
                        console.log(' >>> contabilizarSAP existOneSuccessful >>> ', existOneSuccessful);
                        if (existOneSuccessful) {
                            estado_contabilizacion = constStatus.CONTABILIZACION_OK;
                            return resolve(responseSAP);
                        }

                    }
                    return reject(responseSAP);
                })
                .catch(errServices => {
                    console.log(' >>> Error  >> ', errServices);
                    reject(errServices);
                }).finally(() => {
                    // actualizacion de estado orden
                    orderStatusUpdateNotification(numero_orden, estado_contabilizacion);
                });
        } catch (error) {
            reject(error);
        }
    });
}


/**
 * 
 * @param {*} body 
 * @returns 
 */
async function consumeRfcSap(body) {

    return new Promise(async (resolve, reject) => {
        try {
            if (!has(body, 'functionName'))
                throw new Error("Se requieren los parametros de la funcion para ejecutar el consumo al legado [RFC-SAP]");

            await axios.post(`${gnConfig.ruta_rfc_sap}/sap/invoke`, body)
                .then(response => {
                    let { data } = response;
                    console.log(' Response RFC - SAP > ', data);
                    resolve(data);
                })
                .catch(error => {
                    console.log(' Error en el consumo de RFC - SAP > ', error.message);
                    reject(error);
                });
        } catch (error) {
            console.log(' Error [consumeRfcSap] ', error.message);
            reject(error);
        }
    });
}


function getReqContabilizar(numero_entrega, productos) {

    let functionName = 'Z02RTPMF_0683_CONTABILIZAR_SM';
    let IM_CAB_CONT = []; // numero de entrega
    let IM_POS_CONT = []; // numero de entrega
    let IM_SERIAL_CONT = []; // productos a contrabilizar

    let objNumEntrega = {
        NRO_ENTREGA: numero_entrega
    };

    // se agrega cada serial
    for (let producto of productos) {
        for (let serial of producto.serials) {
            IM_SERIAL_CONT.push({
                NRO_ENTREGA: numero_entrega,
                POS_ENTREGA: producto.position,
                SERIAL: serial
            });
        }
    }

    // asignacion
    IM_CAB_CONT.push(objNumEntrega);
    IM_POS_CONT.push(objNumEntrega);

    return {
        functionName,
        tables: {
            IM_CAB_CONT,
            IM_POS_CONT,
            IM_SERIAL_CONT
        },
        origin: "INTERNAL"
    };

}


module.exports = {
    contabilizarSAP,
};