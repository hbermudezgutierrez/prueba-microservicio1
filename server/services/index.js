/**
 * Componente index.js funciona como el main de todo los servicios
 */

'use strict';

const file = require('./file-sftp.service');
const service_error = require('./services-cache.service');
const orders = require('./order.service');
const pedidosSap = require('./pedidos-sap.service');
const preventa = require('./preventa.service');
const contabilizar = require('./contabilizacion.services');

const services = {
    // service - files
    saveInfoFile: file.saveInfoFile,
    findFile: file.findFile,

    // service - errors
    saveServiceError: service_error.saveServiceError,
    findErrors: service_error.findErrors,

    // service - Orders
    saveOrderBilling: orders.saveOrderBilling,
    findOrdersBilling: orders.findOrdersBilling,
    updateOrderBilling: orders.updateOrderBilling,

    // service - Pedidos sap
    getOrdersDB: pedidosSap.getOrdersDB,
    updatePedidos: pedidosSap.updatePedidos,
    getOrdersCollOrder : pedidosSap.getOrdersCollOrder,

    //preventa 
    updateStatusPresale: preventa.updateStatusPresale,

    contabilizarSAP: contabilizar.contabilizarSAP,

}

module.exports = services;