"use strict";

const Alistamiento = require("../models/facturacion-venta.model"),
    msg = require("../class/message"),
    { setIfNotEmpty} = require("../lib/utilities"),
    gnConfig = require("../config/general.config");
   const servicesErro = require("../models/services-errors.model");
   const axios = require("axios"),
   cypher = require("../lib/cypher");




const UpdateSendByBill = (req,estado) => {
    return new Promise((resolve, reject) => {
            const result = new msg.MessageBuilder()
                .setOrigen(`${req.service}/${req.method}`)
                .setProcess(undefined).build();
                const seteo = {}
               // console.log("estado")
                setIfNotEmpty(seteo, 'Estado_Proceso', estado);
                setIfNotEmpty(seteo, 'Fecha_Proceso', new Date());
                setIfNotEmpty(seteo, 'Valor', req.Valor);
                setIfNotEmpty(seteo, 'Num_factura', req.Num_factura);
                setIfNotEmpty(seteo, 'Fecha_emision', req.Fecha_emision);
                setIfNotEmpty(seteo, 'URL_Factura', req.URL_Factura);
                setIfNotEmpty(seteo, 'Response_error', req.Response_error);
                let query = { 'Num_pedido_SAP': req.Num_pedido_SAP }
                console.log("query",query)
                console.log("seteo",seteo)
                Alistamiento.findOneAndUpdate(query, seteo, (err, response) => {
                if (err) {
                 //   console.log("Error BD Actualizar")
                    result.documents = { "error": err };
                    reject(result);
                } else {
                    if(response!=null){
                       // console.log("Encotro registro BD para actualizar")
                        result.success = true;
                        result.status = 200;
                        result.documents = response;
                        result.message = "Execution successful";
                        resolve(result);
                    }else{
                      //  console.log("No encontro registro para actualizar")
                        //addOrdenAlistamiento(req)
                        result.success = true;
                        result.status = 200;
                        result.documents = response;
                        result.message = "Execution successful";
                        resolve(result);
                    }
                   
                }
            })
        })
    }
    

const createSendByBill= (req)=> {
    return new Promise( async(resolve, reject) => {
        try {
            console.log("ingreso send")
            const result = new msg.MessageBuilder()
                .setOrigen('service/addOrdenAlistamiento')
                .setProcess(undefined).build();
            const seteo = req;
          //  console.log("SETEO:",seteo)
            Alistamiento.insertMany(seteo, (err, resultDB) => {
                if (err) {
                //    console.log("error 1")
                    result.documents = { "n": 1, "nModified": 0, "ok": 0, "error": err };
                    reject(result);
                } else if (resultDB.length === 0) {
                 //   console.log("error 2")
                    result.success = false;
                    result.status = 500;
                    result.documents = resultDB;
                    result.message = "No fue posible crear el documento de Alistamiento";
                    reject(result);
                } else {
                 //   console.log("error 3")
                    result.success = true;
                    result.status = 200;
                    result.documents = resultDB;
                    result.message = "Execution successful";
                    resolve(result);
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

async function requestSendByBill(req) {
    let reqSendByBill = {
      Num_Orden:req.info_factura.Num_Orden,
      Num_pedido_SAP:req.info_factura.Num_pedido_SAP,
      Nom_cliente:req.info_factura.Nom_cliente,
      Num_factura:"",
      Fecha_emision:"",
      Valor:"",
      Email:req.info_factura.Email,
      Tipo_proceso:req.info_factura.Tipo_proceso,
      Canal:req.info_factura.Canal,
      URL_Factura:"",
      Estado_Proceso:gnConfig.estado_recibido,
      Fecha_Proceso:new Date(),
      Response_error:""
    }
      return reqSendByBill
  }

  async function requestServicesErrors(req,res,estado) {
    let reqSerErrors = {
      request:req,
      response:res,
      Estado_Proceso:estado
    }
      return reqSerErrors
  }

  async function requestSAPSendByBill(req) {
    let reqSAPSendByBill = {
        functionName:"BAPISDORDER_GETDETAILEDLIST",
      tables:{
        SALES_DOCUMENTS:[
            {
                VBELN: req.info_factura.Num_pedido_SAP
            }
        ] 
      },
      structures:{
        I_BAPI_VIEW:{
            paramList:{
                FLOW:"X"
            }
        }
      },
      origin:"INTERNAL"
    }
      return reqSAPSendByBill
  }

  async function requestBillingdocGetdetail(numeroFactura) {
    let reqBillingdocGetdetail = {
      functionName: "BAPI_BILLINGDOC_GETDETAIL",
      simpleParams: {
        BILLINGDOCUMENT: numeroFactura,
      },
      structures: {},
      origin: "INTERNAL",
    };
    return reqBillingdocGetdetail;
  }

  async function requestEnvioCorreo(req) {
    let reqSAPSendByBill = { 

        id_process: "1213123412AFC",
        to: req.Email,
        displayname: "Notificación Tienda Virtual Claro",
        subject: "Factura Tienda Virtual Claro",
        attachedUrl: [
            {
                numPedido: req.Num_pedido_SAP,
                factura: req.URL_Factura
            }
        ],
        template_email: {
          id_template: "factura_plantilla",
            content_type: "CONTENTID",
            params: {
              FACTURA :req.Num_factura,
                  FECHA_EMISION:req.Fecha_emision,
                  VALOR :JSON.stringify(req.Valor)           
            }
        }
    }
    return reqSAPSendByBill
  }

  const findSendByBill = (req) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = new msg.MessageBuilder()
        .setOrigen("getLogsAgendamiento").setProcess(undefined).build();
        let filter = {};
        setIfNotEmpty(filter, 'Num_pedido_SAP', req.Num_pedido_SAP);
       // console.log('filter:', filter)
        Alistamiento
            .find(filter)
            .lean()
            .exec((err, resultDB) => {
                if (err) {
                    result.documents = { "error": err };
                    result.length = 0;
                    reject(result);
                }  else if (resultDB.length === 0) {
                    result.success = true;
                    result.status = 200;
                    result.documents = resultDB;
                    result.length = resultDB.length;
                    result.message = "Execution successful";
                    console.log("RESULT",result)
                    resolve(result);

                 } else {
                    result.success = true;
                    result.status = 200;
                    result.documents = resultDB;
                    result.length = resultDB.length;
                    result.message = "Execution successful";
                   // console.log("EXITOSO FIND 1:",resultDB.length)
                    UpdateSendByBill(req,gnConfig.estado_recibido)
                    resolve(result);
                }
            })
        } catch (error) {
            
        }
        
    })
}

const createServiceErros = (req,res,estado_proceso) => {
    return new Promise((resolve, reject) => {
        console.log("Ingreso a crear registro en errores")
            const result = new msg.MessageBuilder()
                .setOrigen('service/addOrdenAlistamiento')
                .setProcess(undefined).build();
            const seteo = {}
            setIfNotEmpty(seteo, 'request', req);
            setIfNotEmpty(seteo, 'response', res);
            setIfNotEmpty(seteo, 'Estado_Proceso', estado_proceso);
            setIfNotEmpty(seteo, 'Fecha_Proceso', new Date());
            console.log("campos a crear:",seteo)
            servicesErro.insertMany(seteo, (err, resultDB) => {
                if (err) {
                    console.log("Fallido BD ERRORS")
                    result.documents = { "n": 1, "nModified": 0, "ok": 0, "error": err };
                    reject(result);
                } else if (resultDB.length === 0) {
                    console.log("Exitoso BD ERRORS")
                    result.success = false;
                    result.status = 500;
                    result.documents = { "n": 1, "nModified": resultDB.length, "ok": 0 };
                    result.message = "No fue posible crear el documento de Alistamiento";
                    reject(result);
                } else {
                    console.log("Exitoso BD ERRORS 2")
                    result.success = true;
                    result.status = 200;
                    result.documents = { "n": 1, "nModified": resultDB.length, "ok": 1 };
                    result.message = "Execution successful";
                    resolve(result);
                }
            });
       
    });
}

const getURLV2 = async req => {
    let result = "";
  try {
      console.log("Request inicial GetUrlV2 >>>",req)
    const token = cypher.encryptV2(`${req}|S`);


    console.log("TOKEN:",token)
    
    let re = await axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: gnConfig.url_paradigma2,
      preambleCRLF: true,
      postambleCRLF: true,
      data: JSON.stringify({data: token,origin:"ECOMMERCE"})
    });

    result = JSON.parse(re.data.d);
    // console.log("RESPONSE>>>>",dataResp)
    /* return

    if (dataResp.error.isError === true) {      
      throw new Error(JSON.parse(re.data.d).error.msg);
    }
    
    if (dataResp.url !== ""){
      result = JSON.parse(re.data.d).url;
      console.log("RESSSSS",result)
    }*/
  } catch (e) {
    if (gnConfig.logs) console.log("Error GetUrlV2 >>>:", e);
    return e
  } finally{
    return result;
  }
};

const getURLSendByBill = async (numero_factura,mail) => {
    let result = "";
  try {
    const token = cypher.encryptV2(`${numero_factura}|${mail}|S`);
    console.log("data obtenida :",token)
    let re = await axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: gnConfig.url_paradigma2,
      preambleCRLF: true,
      postambleCRLF: true,
      data: JSON.stringify({data: token,origin:"ECOMMERCE"})
    });
    result = JSON.parse(re.data.d);
    console.log("DATA",result)
  } catch (e) {
    if (gnConfig.logs) console.log("Error >>>:", e);
  } finally{
    return result;
  }
};

const sendEmailSendByBill= async (req) => {
   try {
   let responseSendEmail = await axios({
       headers: { "content-type": "application/json"},
       method: "post",
       url: `${gnConfig.ruta_notifications}/notification/email`,
       data: req
   });
   console.log(' Response ms-notification >> ', responseSendEmail.data || responseSendEmail);
   
      return responseSendEmail.data;
   
   } catch (e) {
       return e;
       
   }
   
}

const getFacturacionVenta = (req) => {
  return new Promise(async (resolve, reject) => {
    const result = new msg.MessageBuilder()
    .setOrigen('getFacturacionVenta').setProcess(undefined).build();
    let filter = {};
    setIfNotEmpty(filter, 'Estado_Proceso', req.query.estado);
    setIfNotEmpty(filter, 'Num_Orden', req.query.numOrden);
    setIfNotEmpty(filter, 'Num_pedido_SAP', req.query.numPedidoSap);

    if (req.query.startDate && req.query.endDate) {
        filter["createdAt"] = {
            $gte: new Date(`${req.query.startDate} 00:00`),
            $lte: new Date(`${req.query.endDate} 23:59`)
        };
    }

    if (req.query.startDateProcess && req.query.endDateProcess) {
      filter["Fecha_Proceso"] = {
          $gte: new Date(`${req.query.startDateProcess} 00:00`),
          $lte: new Date(`${req.query.endDateProcess} 23:59`)
      };
  }


    console.log('filter:', filter)

    const limit = parseInt(req.query.page_size, 10) || 10;
    const page = parseInt(req.query.page, 10) || 1;
    const skip = (page - 1) * limit;
    const docs = await Alistamiento.find(filter).exec();
    const totalDocs = docs.length;
    Alistamiento
        .find(filter)
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
        .lean()
        .exec((err, resultDB) => {
            if (err) {
                result.documents = { "error": err };
                result.length = 0;
                reject(result);
            } else {
                result.success = true;
                result.status = 200;
                result.documents = resultDB;
                result.length = resultDB.length;
                result.message = "Execution successful";
                result.total_elements = totalDocs;
                resolve(result);
            }
        })
  })
};

const getCollServicesErros = (req) => {
  return new Promise(async (resolve, reject) => {
    const result = new msg.MessageBuilder()
    .setOrigen('getCollServicesErros').setProcess(undefined).build();
    let filter = {};
    if(req.query.numOrden || req.query.numPedidoSap){
      let filterOr = [];
      if(req.query.numOrden !== undefined){
        filterOr.push({'request.info_factura.Num_Orden':req.query.numOrden});
      }
      if(req.query.numPedidoSap !== undefined){
        filterOr.push(
          {'request.info_factura.Num_pedido_SAP':req.query.numPedidoSap},
          {'request.tables.SALES_DOCUMENTS.VBELN':req.query.numPedidoSap},
          {'request.attachedUrl.numPedido':req.query.numPedidoSap}
        );
      }
      console.log('filterOr:',filterOr)
      filter["$or"]=filterOr;
      console.log('filter["$or"]:',filter["$or"])
    }
    if (req.query.startDate && req.query.endDate) {
        filter["createdAt"] = {
            $gte: new Date(`${req.query.startDate} 00:00`),
            $lte: new Date(`${req.query.endDate} 23:59`)
        };
    }

    console.log('filter:', filter)

    const limit = parseInt(req.query.page_size, 10) || 10;
    const page = parseInt(req.query.page, 10) || 1;
    const skip = (page - 1) * limit;
    const docs = await servicesErro.find(filter).exec();
    const totalDocs = docs.length;
    servicesErro
        .find(filter)
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
        .lean()
        .exec((err, resultDB) => {
            if (err) {
                result.documents = { "error": err };
                result.length = 0;
                reject(result);
            } else {
                result.success = true;
                result.status = 200;
                result.documents = resultDB;
                result.length = resultDB.length;
                result.message = "Execution successful";
                result.total_elements = totalDocs;
                resolve(result);
            }
        })
  })
};

module.exports = {
    findSendByBill,
    UpdateSendByBill,
    createSendByBill,
    requestSendByBill,
    requestSAPSendByBill,
    requestBillingdocGetdetail,
    requestServicesErrors,
    createServiceErros,
    getURLV2,
    getURLSendByBill,
    requestEnvioCorreo,
    getFacturacionVenta,
    getCollServicesErros,
    sendEmailSendByBill
};