"use strict";

const axios = require('axios'),
  gnConfig = require("../config/general.config"),
  { isNullOrEmpty, setIfNotEmpty, isEmptyElement } = require('../lib/utilities');

/**
 * Funcion que permite buscar pedidos de SAP
 * @param {*} params filtros para busqueda de documentos
 * @returns [] documents segun el filtro
 */
const getOrdersDB = async (params) => {
  try {
    const { data } = await axios({
      headers: { "content-type": "application/json" },
      method: "get",
      url: `${gnConfig.ruta_orders}/pedido/sap-masivo/${params}`,
      preambleCRLF: true,
      postambleCRLF: true
    });
    return data;
  } catch (e) {
    if (gnConfig.logs) console.log("Error en getOrdersDB:", e);
  }
};


/**
 * Funcion que permite actualizar los pedidos a NOTIFICADO
 * @param {*} req pedidos a actualizar
 */
const updatePedidos = async req => {
  let result = "";
  try {
    console.log("req en updatePedidos:", req)
    let re = await axios({
      headers: { "content-type": "application/json" },
      method: "post",
      url: `${gnConfig.ruta_orders}/pedido/sap-masivo`,
      preambleCRLF: true,
      postambleCRLF: true,
      data: req
    });

    console.log("re:", re.data);
    result = re.data;
  } catch (e) {
    if (gnConfig.logs) console.log("Error >>>:", e);
  } finally {
    return result;
  }
};




const orderStatusUpdateNotification = async (orderNumber, orderStatus) => {
  return new Promise(async (resolve, reject) => {
    try {
      if (isNullOrEmpty(orderNumber) || isNullOrEmpty(orderStatus))
        throw new Error("Se requere un numero de orden y/o estado para actulizar");

      await axios({
        headers: { "content-type": "application/json" },
        method: "put",
        url: `${gnConfig.ruta_orders}/pedido/estado-orden/${orderNumber}`,
        data: { estado: orderStatus }
      }).then((response) => {
        let { data } = response;
        console.log(`Actualizacion de estado [${orderStatus}] de la orden [${orderNumber}] > `, data);
        resolve(data);
      }).catch((err) => {
        reject(err);
      });
    } catch (e) {
      reject(e);
    }
  });
};


async function getOrdersCollOrder(params) {
  const query = {};
  setIfNotEmpty(query, 'id_process', params.id_process);
  setIfNotEmpty(query, 'num_orden', params.num_orden);
  setIfNotEmpty(query, 'id_omnicanal', params.id_omnicanal);
  if (isEmptyElement(query))
    throw new Error("Se requiere un id_process o un num_orden para consultar la orden");
  const { data } = await axios
    .get(`${gnConfig.ruta_orders}/pedido/orden`, { params: query });
  return data;
}


module.exports = {
  getOrdersDB,
  updatePedidos,
  orderStatusUpdateNotification,
  getOrdersCollOrder,
};