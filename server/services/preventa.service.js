'use strict';

const axios = require("axios"),
    { ruta_presale } = require('../config/general.config'),
    { has, isNullOrEmpty } = require('../lib/utilities');


/**
 * Funcion que permite realizar la actualizacion del estado de la orden en ms-presale
 * @param {String} numeroOrden que se desea actualizar
 * @param {{estado: String}} request con estdo que se desea actualizar en la orden 
 * @returns {{}} Con la informacion de la transacion
 */
const updateStatusPresale = async (numeroOrden, request) => {
    return new Promise(async (resolve, reject) => {
        if (!isNullOrEmpty(numeroOrden) && !isNullOrEmpty(request)) {
            await axios.put(`${ruta_presale}/presale/estado/${numeroOrden}`, request)
                .then(res => {
                    console.log(' Response ms-presale >> ', res.data || res);
                    if (has(res, 'data') && has(res.data, 'success') && res.data.success) {
                        return resolve(res.data);
                    } else if (has(res, 'data') && has(res.data, 'message')) {
                        return reject({ message: res.data.message });
                    } else {
                        return reject(res);
                    }
                })
                .catch(e => {
                    console.log(' - Error updateStatusPresale > ', e.message || e);
                    return reject(e);
                });
        } else {
            return reject({ message: 'No se puede actualizar los datos sin numero de orden y/o request.' });
        }
    });
}


module.exports = { updateStatusPresale };