'use strict';

const OrderBillingSchema = require('../models/order-billing.model'),
    { setIfNotEmpty, isNullOrEmpty } = require('../lib/utilities');

/**
 * Funcion que permite el guardado de Orden en la collecion coll_orders-Billing
 * @param {*} dtaOrderBilling Object Model coleccion coll_orders-Billing
 * @returns informacion de guardado
 */
const saveOrderBilling = async (dtaOrderBilling) => {
    return new Promise((resolve, reject) => {
        let orderBilling = new OrderBillingSchema({ ...dtaOrderBilling });
        orderBilling.save(function (err, doc) {
            if (err)
                return reject(err);
            console.log("Document <OrderBillingSchema> inserted successfully!");
            return resolve(doc);
        });
    });
};

/**
 * Funcion que permite buscar los documento de la coleccion coll_orders-Billing
 * @param {*} params filtros para busqueda de documentos
 * @returns [] documents segun el filtro
 */
const findOrdersBilling = async (params) => {
    return new Promise((resolve, reject) => {

        let filter = {};
        setIfNotEmpty(filter, 'id_process', params.id_process);
        setIfNotEmpty(filter, 'numero_orden', params.numero_orden);
        setIfNotEmpty(filter, 'estado', params.estado);
        if (!isNullOrEmpty(params.startDate))
            filter['createdAt'] = { $gte: new Date(`${params.startDate} 00:00`) };
        if (!isNullOrEmpty(params.updateDate))
            filter['updatedAt'] = params.updateDate.length > 8
                ? { $gte: new Date(params.updateDate) }
                : { $gte: new Date(`${params.updateDate} 00:00`) };

        console.log(' filter findOrdersBilling > ', filter);

        OrderBillingSchema.find(filter)
            .lean()
            .exec((err, resultDB) => {
                if (err)
                    return reject(err);
                console.log(' findOrdersBilling > ' + resultDB.length);
                return resolve(resultDB);
            });
    });
};


/**
 * Funcion que permite actulizar estado de documento en le coleccion coll_orders-Billing
 * @param {*} dtaOrder { .. , query {}} informacion de actulizacion y query para filtrar datos de update
 * @returns informacion de actualizacion de documento
 */
const updateOrderBilling = async (dtaOrder) => {

    return new Promise((resolve, reject) => {
        let seteo = {};
        setIfNotEmpty(seteo, 'estado', dtaOrder.estado);
        setIfNotEmpty(seteo, 'factura.numero', dtaOrder.numero_factura);
        setIfNotEmpty(seteo, 'factura.url', dtaOrder.url_factura);

        let query = {};
        setIfNotEmpty(query, 'numero_orden', dtaOrder.query.numero_orden);
        setIfNotEmpty(query, 'numero_pedido', dtaOrder.query.numero_pedido);
        setIfNotEmpty(query, 'id_process', dtaOrder.query.id_process);

        const update = {
            $set: seteo,
            $push: { logs_estados: { estado: dtaOrder.estado, createdAt: new Date().toISOString() } }
        }
        console.log('updateOrderBilling [Query]', JSON.stringify(query), ' [Seteo] ', JSON.stringify(update));

        OrderBillingSchema.updateOne(query, update, { multi: true })
            .exec((err, resultDB) => {
                if (err)
                    return reject(err);
                console.log("Document <OrderBillingSchema> updated successfully!");
                return resolve(resultDB);
            });
    });

}


module.exports = {
    saveOrderBilling,
    findOrdersBilling,
    updateOrderBilling
};