'use strict';

const express = require('express'),
    database = require("./class/database"),
    metricasConfig = require('./config/reportmetricas.config'),
    gnConfig = require('./config/general.config'),
    schedule = require('./class/schedule'),
    app = express(),
    dispatcher = require('./lib/dispatcher.js'),
    swaggerUi = require('swagger-ui-express'),
    apmConfig = require('./config/elastic-apm.config'),
    openApiDocumentation = require('./doc/documentation-swagger.json'),
    { handleOrderInstalled, handleDeliveryOrderEntregado } = require('./handlers/handle-events.service'),
    cors = require('cors'),
    { runJobFacturacion } = require('./job/cronMan'),
    KafkaClass = require('lib-event-kafka-claro'),
    { handlerOfTopicOrderDeliveredCustomerTopic } = require('./handlers/event-handle-delivered.services');


// var apm = require('elastic-apm-node').start();
require('events').defaultMaxListeners = 100;

app
    .use(express.urlencoded({ extended: false }))
    .use(express.json())
    .use(cors())
    .use(require('./routes/index'))
    .use('/factura/v1/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation))
    .use('/factura/v1', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));

let server = app.listen(gnConfig.port, () => {
    console.log("Service <Billing> listening in the port:", gnConfig.port);
});


// Se subscribe a los eventos que van a escuchar y quedan en listen
dispatcher.registerEvent('OrderInstalled', handleOrderInstalled);
dispatcher.registerEvent('msDeliveryOrderEntregadoCliente', handleDeliveryOrderEntregado);


server.on('listening', () => {
    dispatcher.listen();
})

//Jobs
schedule.setCron(gnConfig.job_cron_expression).run();
schedule.setCron(gnConfig.job_cronMail_expression).runMail();
schedule.setCron(metricasConfig.job_cron_report_metricas).getReportMetricas();
schedule.setCron(gnConfig.cron_expression_relaunchFailed).relaunchFailedCron();

runJobFacturacion(); // Job de facturacion - SAP - Paradigma - Notificacion

// event Kafka
KafkaClass.subscribeTopic('OrderDeliveredCustomerTopic', handlerOfTopicOrderDeliveredCustomerTopic);
KafkaClass.consume();

