#FROM clarodigital.azurecr.io/node:8
#FROM node:8
FROM #{Dockerfile.Image}#:#{Dockerfile.NodejsVersion}#
MAINTAINER Hugo Bermudez Gutierrez
COPY . .
EXPOSE $PORT
CMD [ "node", "server/server.js" ]